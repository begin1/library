package queue

import (
	"gitlab.com/begin1/library/base/ctime"
	render "gitlab.com/begin1/library/base/logrender"
	"github.com/streadway/amqp"
)

type EndpointConfig struct {
	Address string `yaml:"address"`
	Port    int    `yaml:"port"`
}

type Config struct {
	// 服务器地址
	Endpoint *EndpointConfig `yaml:"endpoint"`
	UserName string          `yaml:"userName"`
	Password string          `yaml:"password"`
	// 会话配置
	Session map[string]*SessionConfig `yaml:"session"`

	// 连接超时时间
	//	默认为 5s
	ConnectTimeout ctime.Duration `yaml:"connectTimeout"`
	// 当连接失败时重连的延迟
	//	默认为 3s
	ReconnectDelay ctime.Duration `yaml:"reconnectDelay"`
	// 当channel异常时重新初始化的延迟
	//	默认为 3s
	ReInitDelay ctime.Duration `yaml:"reInitDelay"`
	// 服务器没有确认时的重发延迟
	//	默认为 3s
	ResendDelay ctime.Duration `yaml:"resendDelay"`
	// 服务器没有确认时的重发次数
	//	默认为 3
	RetrySendTime int `yaml:"retrySendTime"`

	// 日志配置
	*render.Config `yaml:",inline"`
}

type SessionConfig struct {
	// 队列名
	Name string `yaml:"name"`
	// 交换器名
	ExchangeName string `yaml:"exchangeName"`
	// 路由key
	RoutingKey string `yaml:"routingKey"`
	// 是否持久化
	Durable bool `yaml:"durable"`
	// 是否自动删除
	AutoDelete bool `yaml:"autoDelete"`
	// 是否设置排他
	Exclusive bool `yaml:"exclusive"`
	// 是否非阻塞
	NoWait bool `yaml:"noWait"`
	// 额外参数
	Args amqp.Table `yaml:"args"`

	// 当连接失败时重连的延迟
	reconnectDelay ctime.Duration
	// 当channel异常时重新初始化的延迟
	reInitDelay ctime.Duration
	// 服务器没有确认时的重发延迟
	resendDelay ctime.Duration
	// 服务器没有确认时的重发次数
	retrySendTime int
}
