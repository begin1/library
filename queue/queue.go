package queue

import (
	"fmt"
	"gitlab.com/begin1/library/base/ctime"
	render "gitlab.com/begin1/library/base/logrender"
	"github.com/pkg/errors"
	"time"
)

// AMQP客户端
type Queue struct {
	// 配置文件
	config *Config
}

func New(config *Config) *Queue {
	if config == nil {
		panic("queue config is nil")
	}

	if config.Config == nil {
		config.Config = &render.Config{}
	}
	if config.ConnectTimeout == 0 {
		config.ConnectTimeout = ctime.Duration(time.Second * 5)
	}
	if config.ReconnectDelay == 0 {
		config.ReconnectDelay = ctime.Duration(time.Second * 3)
	}
	if config.ReInitDelay == 0 {
		config.ReInitDelay = ctime.Duration(time.Second * 3)
	}
	if config.ResendDelay == 0 {
		config.ResendDelay = ctime.Duration(time.Second * 30)
	}
	if config.RetrySendTime == 0 {
		config.RetrySendTime = 3
	}

	queue := &Queue{
		config: config,
	}

	return queue
}

func getConnectAddr(cfg *Config) string {
	return fmt.Sprintf("amqp://%s:%s@%s:%d/", cfg.UserName, cfg.Password,
		cfg.Endpoint.Address, cfg.Endpoint.Port)
}

// 新建会话
func (q *Queue) NewSession(name string) (*Session, error) {
	sc, ok := q.config.Session[name]
	if !ok {
		return nil, errors.New(fmt.Sprintf("Session %s haven't config", name))
	}

	sc.reInitDelay = q.config.ReInitDelay
	sc.reconnectDelay = q.config.ReconnectDelay
	sc.resendDelay = q.config.ResendDelay
	sc.retrySendTime = q.config.RetrySendTime

	session := Session{
		config: sc,
		logger: getDefaultWriter(q.config),
		done:   make(chan bool),
	}

	// 开启重连协程
	go session.handleReconnect(getConnectAddr(q.config))

	// 等待初始化成功
	connectTimeout := time.NewTicker(time.Duration(q.config.ConnectTimeout))
	checkReady := time.NewTicker(time.Millisecond * 500)
	for {
		if session.isReady {
			return &session, nil
		}

		select {
		case <-connectTimeout.C:
			session.Close()
			return nil, errNotConnected
		case <-checkReady.C:
			continue
		}
	}
}
