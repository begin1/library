package queue

import (
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"time"
)

// 会话
type Session struct {
	// 配置文件
	config *SessionConfig
	// 日志logger
	logger logger
	// 连接
	connection *amqp.Connection
	// channel通道
	channel *amqp.Channel
	// 会话是否关闭
	done chan bool
	// 通知连接关闭
	notifyConnClose chan *amqp.Error
	// 通知通道关闭
	notifyChanClose chan *amqp.Error
	// 通知确认收到消息
	notifyConfirm chan amqp.Confirmation
	// 通道是否可用
	isReady bool
}

var (
	// 连接错误
	errNotConnected = errors.New("not connected to a server")
	// 连接已关闭错误
	errAlreadyClosed = errors.New("already closed: not connected to the server")
	// 会话关闭错误
	errShutdown = errors.New("session is shutting down")
)

// 打印日志
func (session *Session) log(values ...interface{}) {
	msg := make(map[string]interface{})
	msg["queue_name"] = session.config.Name
	msg["message"] = values[0]
	msg["error"] = values[1]

	session.logger.Print(msg)
}

// 处理重连
func (session *Session) handleReconnect(addr string) {
	for {
		session.isReady = false

		conn, err := session.connect(addr)
		// 连接错误
		if err != nil {
			session.log("Failed to connect. Retrying...", err)

			select {
			case <-session.done:
				return
			case <-time.After(time.Duration(session.config.reconnectDelay)):
			}

			continue
		}

		// 初始化
		if done := session.handleReInit(conn); done {
			break
		}
	}
}

// 连接
func (session *Session) connect(addr string) (*amqp.Connection, error) {
	conn, err := amqp.Dial(addr)
	if err != nil {
		return nil, err
	}

	session.changeConnection(conn)

	return conn, nil
}

// 处理重新初始化
func (session *Session) handleReInit(conn *amqp.Connection) bool {
	for {
		session.isReady = false

		err := session.init(conn)
		// 初始化错误
		if err != nil {
			session.log("Failed to initialize channel. Retrying...", err)

			select {
			case <-session.done:
				return true
			case <-time.After(time.Duration(session.config.reInitDelay)):
			}

			continue
		}

		select {
		case <-session.done:
			return true
		case <-session.notifyConnClose:
			session.log("Connection closed. Reconnecting...", nil)
			return false
		case <-session.notifyChanClose:
			session.log("Channel closed. Re-running init...", nil)
		}
	}
}

// 初始化队列
func (session *Session) init(conn *amqp.Connection) error {
	ch, err := conn.Channel()
	if err != nil {
		return err
	}

	err = ch.Confirm(false)
	if err != nil {
		return err
	}

	_, err = ch.QueueDeclare(
		session.config.Name,
		session.config.Durable,
		session.config.AutoDelete,
		session.config.Exclusive,
		session.config.NoWait,
		session.config.Args,
	)
	if err != nil {
		return err
	}

	session.changeChannel(ch)
	session.isReady = true

	return nil
}

// 更改连接
func (session *Session) changeConnection(connection *amqp.Connection) {
	session.connection = connection
	session.notifyConnClose = make(chan *amqp.Error)
	session.connection.NotifyClose(session.notifyConnClose)
}

// 更改通道
func (session *Session) changeChannel(channel *amqp.Channel) {
	session.channel = channel
	session.notifyChanClose = make(chan *amqp.Error)
	session.channel.NotifyClose(session.notifyChanClose)
	session.notifyConfirm = make(chan amqp.Confirmation, 1)
	session.channel.NotifyPublish(session.notifyConfirm)
}

// 获取当前通道
func (session *Session) GetChannel() (*amqp.Channel, error) {
	if !session.isReady {
		return nil, errNotConnected
	}

	return session.channel, nil
}

// 发送消息，若没有收到确认，会重新发送，直到收到确认消息
func (session *Session) Push(mandatory, immediate bool, msg amqp.Publishing) error {
	if !session.isReady {
		return errors.New("failed to push: not connected")
	}

	resendTime := 0
	limitResendTime := session.config.retrySendTime

	for {
		// 达到限定重发次数
		if limitResendTime != 0 && resendTime > limitResendTime {
			return errors.New("failed to push: reach retry send limit count")
		}

		err := session.UnsafePush(mandatory, immediate, msg)
		// 发送错误
		if err != nil {
			session.log("Push failed. Retrying...", err)

			select {
			case <-session.done:
				return errShutdown
			case <-time.After(time.Duration(session.config.resendDelay)):
			}

			continue
		}

		select {
		case confirm := <-session.notifyConfirm:
			// 确认收到
			if confirm.Ack {
				return nil
			}
		case <-time.After(time.Duration(session.config.resendDelay)):
		}

		session.log("Push didn't confirm. Retrying...", nil)
		resendTime++
	}
}

// 发送消息，不会确认收到消息
func (session *Session) UnsafePush(mandatory, immediate bool, msg amqp.Publishing) error {
	if !session.isReady {
		return errNotConnected
	}

	return session.channel.Publish(
		session.config.ExchangeName,
		session.config.RoutingKey,
		mandatory,
		immediate,
		msg,
	)
}

// 通道消息消费流
func (session *Session) Stream(consumer string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table) (<-chan amqp.Delivery, error) {
	if !session.isReady {
		return nil, errNotConnected
	}

	return session.channel.Consume(
		session.config.Name,
		consumer,
		autoAck,
		exclusive,
		noLocal,
		noWait,
		args,
	)
}

// 获取非自动回复消费流
func (session *Session) NoAutoAckConsumeStream(prefetchCount, prefetchSize int, global bool) (<-chan amqp.Delivery, error) {
	if !session.isReady {
		return nil, errNotConnected
	}

	// 绑定队列
	err := session.channel.QueueBind(session.config.Name, session.config.RoutingKey, session.config.ExchangeName, true, nil)
	if err != nil {
		return nil, err
	}

	// 设置通道消费速度
	err = session.channel.Qos(prefetchCount, prefetchSize, global)
	if err != nil {
		return nil, err
	}

	return session.Stream(
		"",
		false,
		false,
		false,
		false,
		nil,
	)
}

// 简单获取消费流
func (session *Session) SingleConsumeStream() (<-chan amqp.Delivery, error) {
	if !session.isReady {
		return nil, errNotConnected
	}

	// 绑定队列
	err := session.channel.QueueBind(session.config.Name, session.config.RoutingKey, session.config.ExchangeName, true, nil)
	if err != nil {
		return nil, err
	}

	// 设置通道消费速度
	err = session.channel.Qos(1, 0, true)
	if err != nil {
		return nil, err
	}

	return session.Stream(
		"",
		true,
		false,
		false,
		false,
		nil,
	)
}

// 关闭会话
func (session *Session) Close() error {
	defer func() {
		// 通知关闭
		close(session.done)
		session.isReady = false

		session.logger.Close()
	}()

	if !session.isReady {
		return errAlreadyClosed
	}

	// 关闭通道
	err := session.channel.Close()
	if err != nil {
		return err
	}
	// 关闭连接
	err = session.connection.Close()
	if err != nil {
		return err
	}

	return nil
}
