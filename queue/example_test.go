package queue

import (
	"fmt"
	"gitlab.com/begin1/library/base/ctime"
	render "gitlab.com/begin1/library/base/logrender"
	"github.com/streadway/amqp"
	"golang.org/x/sync/errgroup"
	"time"
)

func ExamplePush() {
	conf := &Config{
		Endpoint: &EndpointConfig{
			Address: "localhost",
			Port:    5672,
		},
		UserName:       "guest",
		Password:       "password",
		ConnectTimeout: ctime.Duration(time.Second * 5),
		ReconnectDelay: ctime.Duration(time.Second * 3),
		ReInitDelay:    ctime.Duration(time.Second * 3),
		ResendDelay:    ctime.Duration(time.Second * 5),
		Session: map[string]*SessionConfig{
			"test": {
				Name:         "test_queue",
				ExchangeName: "test",
				RoutingKey:   "",
				Durable:      true,
				AutoDelete:   false,
				Exclusive:    false,
				NoWait:       false,
			},
		},
		Config: &render.Config{
			Stdout: false,
		},
	}

	client := New(conf)
	session, err := client.NewSession("test")
	if err != nil {
		return
	}

	err = session.Push(false, false, amqp.Publishing{
		ContentType: "text/plain",
		Body:        []byte("this is a test"),
	})
	if err != nil {
		return
	}

	wg := new(errgroup.Group)
	wg.Go(func() error {
		msgChannel, err := session.SingleConsumeStream()
		if err != nil {
			return err
		}

		for d := range msgChannel {
			s := string(d.Body)
			fmt.Printf("%s\n", s)
			// 仅Example使用，实际使用中请勿return
			return nil
		}
		return nil
	})
	err = wg.Wait()
	if err != nil {
		fmt.Errorf("%s\n", err)
	}

	session.Close()
}

func ExampleStream() {
	conf := &Config{
		Endpoint: &EndpointConfig{
			Address: "localhost",
			Port:    5672,
		},
		UserName:       "guest",
		Password:       "password",
		ConnectTimeout: ctime.Duration(time.Second * 5),
		ReconnectDelay: ctime.Duration(time.Second * 3),
		ReInitDelay:    ctime.Duration(time.Second * 3),
		ResendDelay:    ctime.Duration(time.Second * 5),
		Session: map[string]*SessionConfig{
			"test": {
				Name:         "test_queue",
				ExchangeName: "test",
				RoutingKey:   "",
				Durable:      true,
				AutoDelete:   false,
				Exclusive:    false,
				NoWait:       false,
			},
		},
		Config: &render.Config{
			Stdout: false,
		},
	}

	client := New(conf)
	session, err := client.NewSession("test")
	if err != nil {
		return
	}

	err = session.Push(false, false, amqp.Publishing{
		ContentType: "text/plain",
		Body:        []byte("this is a test"),
	})
	if err != nil {
		return
	}

	wg := new(errgroup.Group)
	wg.Go(func() error {
		channel, err := session.GetChannel()
		if err != nil {
			return err
		}

		err = channel.QueueBind(session.config.Name, session.config.RoutingKey, session.config.ExchangeName, true, nil)
		if err != nil {
			return err
		}

		err = channel.Qos(1, 0, true)
		if err != nil {
			return err
		}

		msgChannel, err := session.Stream("", true, false, false, false, nil)
		if err != nil {
			return err
		}

		for d := range msgChannel {
			s := string(d.Body)
			fmt.Printf("%s\n", s)
			// 仅Example使用，实际使用中请勿return
			return nil
		}
		return nil
	})
	err = wg.Wait()
	if err != nil {
		fmt.Errorf("%s\n", err)
	}

	session.Close()
}
