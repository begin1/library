package queue

import (
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"io"
	"os"
	"path/filepath"
	"time"
)

const (
	_infoFile = "queueInfo.log"

	defaultPattern = "%J{tTUA}"
)

type logger interface {
	Print(m map[string]interface{})
	Close()
}

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
}

func (logger Logger) Print(m map[string]interface{}) {
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Close() {
	for _, writer := range logger.writers {
		writer.Render.Close()
	}
}

func getDefaultWriter(conf *Config) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers: writers,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"U": contextUUID,
	"t": title,
	"A": amqpExtra,
	"N": queueName,
	"M": message,
	"E": errorMessage,
}

// 当前时间
func longTime(map[string]interface{}) (string, interface{}) {
	return "time", time.Now().Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "AMQP"
}

// context中的uuid
func contextUUID(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args["uuid"].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 汇总的queue参数
func amqpExtra(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){queueName, message, errorMessage}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "amqp", m
}

// 队列名
func queueName(args map[string]interface{}) (string, interface{}) {
	queueName, ok := args["queue_name"].(string)
	if !ok {
		return "queue_name", "unknown"
	}

	return "queue_name", queueName
}

// 消息
func message(args map[string]interface{}) (string, interface{}) {
	return "message", args["message"]
}

// 错误消息
func errorMessage(args map[string]interface{}) (string, interface{}) {
	return "error", args["error"]
}
