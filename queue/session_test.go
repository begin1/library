package queue

import (
	"fmt"
	"gitlab.com/begin1/library/base/ctime"
	render "gitlab.com/begin1/library/base/logrender"
	"github.com/streadway/amqp"
	"github.com/stretchr/testify/assert"
	"golang.org/x/sync/errgroup"
	"os"
	"runtime"
	"runtime/pprof"
	"sync"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	code := m.Run()
	os.Exit(code)
}

func TestSession_ReSendPush(t *testing.T) {
	conf := &Config{
		Endpoint: &EndpointConfig{
			Address: "localhost",
			Port:    5672,
		},
		UserName:       "guest",
		Password:       "guest",
		ConnectTimeout: ctime.Duration(time.Second * 5),
		ReconnectDelay: ctime.Duration(time.Millisecond * 5000),
		ReInitDelay:    ctime.Duration(time.Millisecond * 5000),
		ResendDelay:    ctime.Duration(time.Nanosecond * 1),
		Session: map[string]*SessionConfig{
			"test": {
				Name:         "test_queue",
				ExchangeName: "test",
				RoutingKey:   "",
				Durable:      true,
				AutoDelete:   false,
				Exclusive:    false,
				NoWait:       false,
			},
		},
		Config: &render.Config{
			Stdout: false,
		},
	}

	t.Run("safe", func(t *testing.T) {
		t.Skipf("should have amqp environment\n")

		client := New(conf)
		session, err := client.NewSession("test")
		if err != nil {
			panic(err)
		}

		go func() {
			t := time.NewTicker(time.Millisecond * 100)
			for {
				select {
				case <-t.C:
					if runtime.NumGoroutine() > 5 {
						pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
					}
				}
			}
		}()

		time.Sleep(time.Second * 5)
		wg := new(sync.WaitGroup)
		for i := 0; i < 1000; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				err = session.Push(false, false, amqp.Publishing{
					ContentType: "text/plain",
					Body:        []byte("this is a test"),
				})
				if err != nil {
					fmt.Println("Error ", err)
				}
			}()
		}
		time.Sleep(time.Second)
		wg.Wait()
	})

	t.Run("unsafe", func(t *testing.T) {
		t.Skipf("should have amqp environment\n")

		client := New(conf)
		session, err := client.NewSession("test")
		if err != nil {
			panic(err)
		}

		go func() {
			t := time.NewTicker(time.Millisecond * 100)
			for {
				select {
				case <-t.C:
					if runtime.NumGoroutine() > 5 {
						pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
					}
				}
			}
		}()

		time.Sleep(time.Second * 5)
		wg := new(sync.WaitGroup)
		for i := 0; i < 1000; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				err = session.UnsafePush(false, false, amqp.Publishing{
					ContentType: "text/plain",
					Body:        []byte("this is a test"),
				})
				if err != nil {
					fmt.Println("Error ", err)
				}
			}()
		}
		time.Sleep(time.Second)
		wg.Wait()
	})
}

func TestSession_Push(t *testing.T) {
	conf := &Config{
		Endpoint: &EndpointConfig{
			Address: "localhost",
			Port:    5672,
		},
		UserName:       "guest",
		Password:       "password",
		ConnectTimeout: ctime.Duration(time.Second * 5),
		ReconnectDelay: ctime.Duration(time.Second * 3),
		ReInitDelay:    ctime.Duration(time.Second * 3),
		ResendDelay:    ctime.Duration(time.Second * 5),
		Session: map[string]*SessionConfig{
			"test": {
				Name:         "test_queue",
				ExchangeName: "test",
				RoutingKey:   "",
				Durable:      true,
				AutoDelete:   false,
				Exclusive:    false,
				NoWait:       false,
			},
		},
		Config: &render.Config{
			Stdout: true,
		},
	}

	t.Run("normal", func(t *testing.T) {
		t.Skipf("should have amqp environment\n")

		client := New(conf)
		session, err := client.NewSession("test")
		assert.Nil(t, err)

		err = session.Push(false, false, amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("this is a test"),
		})
		assert.Nil(t, err)

		wg := new(errgroup.Group)
		wg.Go(func() error {
			msgChannel, err := session.SingleConsumeStream()
			assert.Nil(t, err)
			for d := range msgChannel {
				s := string(d.Body)
				assert.Equal(t, "this is a test", s)
				return nil
			}
			return nil
		})
		err = wg.Wait()
		assert.Nil(t, err)

		session.Close()
	})

	t.Run("goroutine leak", func(t *testing.T) {
		beforeCount := runtime.NumGoroutine()

		client := New(conf)
		_, _ = client.NewSession("test")

		time.Sleep(time.Second * 3)

		assert.Equal(t, beforeCount, runtime.NumGoroutine())
	})
}

func TestSession_Stream(t *testing.T) {
	t.Skipf("should have amqp environment\n")

	conf := &Config{
		Endpoint: &EndpointConfig{
			Address: "localhost",
			Port:    5672,
		},
		UserName:       "guest",
		Password:       "password",
		ConnectTimeout: ctime.Duration(time.Second * 5),
		ReconnectDelay: ctime.Duration(time.Second * 3),
		ReInitDelay:    ctime.Duration(time.Second * 3),
		ResendDelay:    ctime.Duration(time.Second * 5),
		Session: map[string]*SessionConfig{
			"test": {
				Name:         "test_queue",
				ExchangeName: "test",
				RoutingKey:   "",
				Durable:      true,
				AutoDelete:   false,
				Exclusive:    false,
				NoWait:       false,
			},
		},
		Config: &render.Config{
			Stdout: true,
		},
	}

	t.Run("normal", func(t *testing.T) {
		client := New(conf)
		session, err := client.NewSession("test")
		assert.Nil(t, err)

		err = session.Push(false, false, amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("this is a test"),
		})
		assert.Nil(t, err)

		wg := new(errgroup.Group)
		wg.Go(func() error {
			channel, err := session.GetChannel()
			if err != nil {
				return err
			}

			err = channel.QueueBind(session.config.Name, session.config.RoutingKey, session.config.ExchangeName, true, nil)
			assert.Nil(t, err)

			err = channel.Qos(1, 0, true)
			assert.Nil(t, err)

			msgChannel, err := session.Stream(
				"",
				true,
				false,
				false,
				false,
				nil,
			)
			assert.Nil(t, err)

			for d := range msgChannel {
				s := string(d.Body)
				assert.Equal(t, "this is a test", s)
				return nil
			}
			return nil
		})
		err = wg.Wait()
		assert.Nil(t, err)

		session.Close()
	})
}
