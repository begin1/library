package kafka

import (
	"fmt"
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"io"
	"os"
	"path/filepath"
	"time"
)

const (
	_infoFile = "kafkaInfo.log"

	defaultPattern = "%J{tTK}"
)

type logger interface {
	Print(v ...interface{})
	Printf(format string, v ...interface{})
	Println(v ...interface{})
	Close()
}

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
}

func (logger Logger) Print(v ...interface{}) {
	m := make(map[string]interface{})
	m["message"] = v

	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Printf(format string, v ...interface{}) {
	m := make(map[string]interface{})
	m["message"] = []string{fmt.Sprintf(format, v...)}

	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Println(v ...interface{}) {
	m := make(map[string]interface{})
	m["message"] = v

	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Close() {
	for _, writer := range logger.writers {
		writer.Render.Close()
	}
}

func getDefaultWriter(conf *Config) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers: writers,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"t": title,
	"K": kafkaExtra,
	"M": message,
}

// 当前时间
func longTime(map[string]interface{}) (string, interface{}) {
	return "time", time.Now().Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "Kafka"
}

// 汇总的kafka参数
func kafkaExtra(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){message}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "kafka", m
}

// 消息
func message(args map[string]interface{}) (string, interface{}) {
	return "message", args["message"]
}
