package render

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"sync/atomic"
	"time"
)

// 新建渲染器
// 	patternMap为用于渲染的函数字典，key为用于渲染的格式化字符，值为渲染函数
//	format为渲染的模版
func NewPatternRender(patternMap map[string]func(args map[string]interface{}) (key string, value interface{}), format string) Render {
	// J为保留格式化字符，不可使用
	if _, ok := patternMap["J"]; ok {
		panic("pattern map shouldn't use 'J'")
	}

	p := &pattern{
		renderBufPool: sync.Pool{
			New: func() interface{} {
				return &renderBuffer{
					buffer: new(bytes.Buffer),
				}
			},
		},
		// 缓冲区管道大小应根据服务请求量配置，以防止阻塞
		renderBufChan: make(chan *renderBuffer, 1024),
		// 如开启异步写入，刷新时间应在1s以上，否则会因为频繁写入，更加影响打印时间
		flushTime: 0 * time.Millisecond,
	}

	b := make([]byte, 0, len(format))
	for i := 0; i < len(format); i++ {
		if format[i] != '%' {
			b = append(b, format[i])
			continue
		}
		if i+1 >= len(format) {
			b = append(b, format[i])
			continue
		}

		var curFunc func(map[string]interface{}, *bytes.Buffer) (bool, string, interface{})
		// 渲染json
		if format[i+1] == 'J' && i+2 < len(format) && format[i+2] == '{' {
			jsonFuncArray := make([]func(map[string]interface{}) (string, interface{}), 0)
			for i = i + 2; i+1 < len(format) && format[i+1] != '}'; i++ {
				curFormat := string(format[i+1])
				f, ok := patternMap[curFormat]
				if !ok {
					continue
				}
				jsonFuncArray = append(jsonFuncArray, f)
			}
			curFunc = jsonFormatFactory(jsonFuncArray)
		} else { // 普通渲染
			f, ok := patternMap[string(format[i+1])]
			if !ok {
				b = append(b, format[i])
				continue
			}
			curFunc = convertFunc(f)
		}
		// 非格式化字符，使用普通文本渲染
		if len(b) != 0 {
			p.funcs = append(p.funcs, textFactory(string(b)))
			b = b[:0]
		}

		p.funcs = append(p.funcs, curFunc)
		i++
	}

	if len(b) != 0 {
		p.funcs = append(p.funcs, textFactory(string(b)))
	}

	// 若为定时异步写入，启动守护协程
	if p.flushTime > 0 {
		p.wg.Add(1)
		go p.daemon()
	}

	return p
}

// 渲染buffer结构体
type renderBuffer struct {
	// 数据buffer
	buffer *bytes.Buffer
	// 写入writer
	writer io.Writer
}

// 渲染模版
type pattern struct {
	// 渲染函数
	funcs []func(args map[string]interface{}, buf *bytes.Buffer) (isRender bool, key string, value interface{})
	// 渲染buffer缓冲池
	renderBufPool sync.Pool
	// 协程wait group
	wg sync.WaitGroup
	// 渲染buffer管道
	renderBufChan chan *renderBuffer
	// 是否关闭
	closed int32
	// 定时异步写入时间
	flushTime time.Duration
}

// 定时异步写入守护方法
// 	异步写入相对于同步写入大约减少20%CPU运行时间，内存占用大约额外提高30%
// 	具体对比数值要根据实际刷新时间来决定
//	目前暂不建议开启异步写入，cpu提升不明显，且占用更高内存
//	绝大部分日志库也不带异步写入功能，所以开启后，也较难注入进其他依赖库中
func (p *pattern) daemon() {
	// 待写入buffer区
	sumBuf := renderBuffer{
		buffer: new(bytes.Buffer),
	}
	// 写入定时器
	writeTicker := time.NewTicker(p.flushTime)
	var err error
	for {
		select {
		// 从缓冲区读取数据塞入待写入区中，并重用buf
		case buf, ok := <-p.renderBufChan:
			if ok {
				sumBuf.writer = buf.writer
				sumBuf.buffer.Write(buf.buffer.Bytes())

				buf.buffer.Reset()
				buf.writer = nil
				p.renderBufPool.Put(buf)
			}
		// 从待写入区读取并写入，并重置待写入区
		case <-writeTicker.C:
			if sumBuf.buffer.Len() > 0 {
				if _, err = sumBuf.buffer.WriteTo(sumBuf.writer); err != nil {
					fmt.Printf("%#v\n", err)
				}

				sumBuf.buffer.Reset()
				sumBuf.writer = nil
			}
		}
		// 检查是否关闭
		if atomic.LoadInt32(&p.closed) != 1 {
			continue
		}
		// 关闭，写入剩下的数据
		if _, err = sumBuf.buffer.WriteTo(sumBuf.writer); err != nil {
			fmt.Printf("%#v\n", err)
		}
		for buf := range p.renderBufChan {
			if _, err = buf.buffer.WriteTo(buf.writer); err != nil {
				fmt.Printf("%#v\n", err)
			}

			buf.buffer.Reset()
			buf.writer = nil
			p.renderBufPool.Put(buf)
		}
		break
	}
	p.wg.Done()
}

// 渲染写入writer中
func (p *pattern) Render(w io.Writer, d map[string]interface{}) error {
	buf := p.renderBufPool.Get().(*renderBuffer)

	for _, f := range p.funcs {
		isRender, _, v := f(d, buf.buffer)
		if !isRender {
			buf.buffer.WriteString(fmt.Sprint(v))
		}
	}
	buf.buffer.WriteString("\n")
	buf.writer = w

	var err error

	if p.flushTime > 0 {
		// 定时异步写入
		p.renderBufChan <- buf
	} else {
		// 立即写入
		_, err = buf.buffer.WriteTo(w)

		buf.buffer.Reset()
		p.renderBufPool.Put(buf)
	}

	return err
}

// 获取渲染字符串
func (p *pattern) RenderString(d map[string]interface{}) string {
	buf := p.renderBufPool.Get().(*renderBuffer)

	for _, f := range p.funcs {
		isRender, _, v := f(d, buf.buffer)
		if !isRender {
			buf.buffer.WriteString(fmt.Sprint(v))
		}
	}
	buf.buffer.WriteString("\n")

	return buf.buffer.String()
}

// 关闭渲染器，异步定时写入时必须
func (p *pattern) Close() error {
	atomic.StoreInt32(&p.closed, 1)
	close(p.renderBufChan)
	p.wg.Wait()
	return nil
}

// 外部函数转换
func convertFunc(unwrapFunc func(map[string]interface{}) (string, interface{})) func(map[string]interface{}, *bytes.Buffer) (bool, string, interface{}) {
	return func(args map[string]interface{}, buffer *bytes.Buffer) (isRender bool, key string, value interface{}) {
		key, value = unwrapFunc(args)
		return false, key, value
	}
}

// 普通文本渲染
func textFactory(text string) func(map[string]interface{}, *bytes.Buffer) (bool, string, interface{}) {
	return func(map[string]interface{}, *bytes.Buffer) (bool, string, interface{}) {
		return false, "", text
	}
}

// json渲染map对象池
var jsonFormatMapPool = sync.Pool{
	New: func() interface{} {
		return make(map[string]interface{})
	},
}

// json渲染buffer对象池
var jsonFormatBufferPool = sync.Pool{
	New: func() interface{} {
		return new(bytes.Buffer)
	},
}

// json渲染
func jsonFormatFactory(funArray []func(map[string]interface{}) (string, interface{})) func(map[string]interface{}, *bytes.Buffer) (bool, string, interface{}) {
	return func(args map[string]interface{}, buf *bytes.Buffer) (bool, string, interface{}) {
		jsonMap := jsonFormatMapPool.Get().(map[string]interface{})
		for k := range jsonMap {
			delete(jsonMap, k)
		}

		jsonBuf := jsonFormatBufferPool.Get().(*bytes.Buffer)
		jsonBuf.Reset()

		for _, f := range funArray {
			k, v := f(args)
			if k != "" {
				jsonMap[k] = v
			}
		}
		err := json.NewEncoder(jsonBuf).Encode(jsonMap)
		if err != nil {
			return false, "json", jsonMap
		}

		// todo:json encoder方法会自动追加换行符，真的自作聪明
		//  为了去掉换行符，所以使用额外的buffer做中间转换，有额外内存开销
		b := jsonBuf.Bytes()
		buf.Write(b[:len(b)-1])

		jsonFormatMapPool.Put(jsonMap)
		jsonFormatBufferPool.Put(jsonBuf)

		// 已处理，无需返回value
		return true, "json", nil
	}
}
