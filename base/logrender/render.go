package render

import "io"

// 渲染接口
type Render interface {
	// 渲染写入writer中
	Render(io.Writer, map[string]interface{}) error
	// 获取渲染字符串
	RenderString(map[string]interface{}) string
	// 关闭渲染器，异步定时写入时必须
	Close() error
}
