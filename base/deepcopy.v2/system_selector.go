package deepcopy

import (
	"github.com/pkg/errors"
	"reflect"
	"strconv"
)

// 选择器方法
type SelectorFunc func(dc *DeepCopier, src, dst reflect.Value, dstIndex int) reflect.Value

// default标签选择器
var DefaultTagSelector = func(dc *DeepCopier, src, dst reflect.Value, dstIndex int) reflect.Value {
	defaultName, ok := dc.curDstTags[DefaultMethodName]
	if !ok {
		return dc.NextSelector(src, dst, dstIndex)
	}

	curDst := dst
	// 判断结构体函数函数
	switch curDst.Type().Kind() {
	case reflect.Struct:
		// 指针的方法会包含结构体的方法
		curDst = reflect.New(curDst.Type())
		curDst.Elem().Set(dst)
		if !curDst.IsValid() {
			break
		}
		fallthrough
	case reflect.Ptr:
		for i := 0; i < curDst.Type().NumMethod(); i++ {
			dstMethodValue := curDst.Method(i)

			if !dstMethodValue.IsValid() {
				continue
			}

			if curDst.Type().Method(i).Name != defaultName {
				continue
			}

			result := dstMethodValue.Call([]reflect.Value{reflect.ValueOf(dc.args)})
			if len(result) < 1 {
				dc.SetError(errors.Errorf("`default` tag method:%s return empty", defaultName))
				return dc.DefaultReflectValue()
			} else if len(result) > 1 {
				err, ok := result[len(result)-1].Interface().(error)
				if ok && err != nil {
					dc.SetError(err)
					return dc.DefaultReflectValue()
				}
			}

			return result[0]
		}
	}

	// 判断默认值
	switch dst.Field(dstIndex).Type().Kind() {
	case reflect.Bool:
		boolResult, err := strconv.ParseBool(defaultName)
		if err != nil {
			dc.SetError(errors.Wrapf(err, "`default` tag value:%s error", defaultName))
			return dc.DefaultReflectValue()
		}
		return reflect.ValueOf(boolResult)
	case reflect.String:
		return reflect.ValueOf(defaultName)
	case reflect.Float64:
		floatResult, err := strconv.ParseFloat(defaultName, 64)
		if err != nil {
			dc.SetError(errors.Wrapf(err, "`default` tag value:%s error", defaultName))
			return dc.DefaultReflectValue()
		}
		return reflect.ValueOf(floatResult)
	case reflect.Int64:
		intResult, err := strconv.ParseInt(defaultName, 0, 64)
		if err != nil {
			dc.SetError(errors.Wrapf(err, "`default` tag value:%s error", defaultName))
			return dc.DefaultReflectValue()
		}
		return reflect.ValueOf(intResult)
	case reflect.Uint64:
		uintResult, err := strconv.ParseUint(defaultName, 0, 64)
		if err != nil {
			dc.SetError(errors.Wrapf(err, "`default` tag value:%s error", defaultName))
			return dc.DefaultReflectValue()
		}
		return reflect.ValueOf(uintResult)
	case reflect.Uint:
		uintResult, err := strconv.ParseUint(defaultName, 0, 64)
		if err != nil {
			dc.SetError(errors.Wrapf(err, "`default` tag value:%s error", defaultName))
			return dc.DefaultReflectValue()
		}
		return reflect.ValueOf(uint(uintResult))
	case reflect.Int:
		intResult, err := strconv.Atoi(defaultName)
		if err != nil {
			dc.SetError(errors.Wrapf(err, "`default` tag value:%s error", defaultName))
			return dc.DefaultReflectValue()
		}
		return reflect.ValueOf(intResult)
	}

	if dc.config.StrictMode {
		dc.SetError(errors.Errorf("`default` tag %s couldn't find any valid method or field type", defaultName))
		return dc.DefaultReflectValue()
	}

	return dc.NextSelector(src, dst, dstIndex)
}

// method标签选择器
var MethodTagSelector = func(dc *DeepCopier, src, dst reflect.Value, dstIndex int) reflect.Value {
	if !src.IsValid() {
		return dc.NextSelector(src, dst, dstIndex)
	}

	methodName, ok := dc.curDstTags[FieldMethodTagName]
	if !ok {
		return dc.NextSelector(src, dst, dstIndex)
	}

	switch src.Type().Kind() {
	case reflect.Struct:
		// 指针的方法会包含结构体的方法
		ptrSrc := reflect.New(src.Type())
		ptrSrc.Elem().Set(src)
		res := dc.CurrentSelector(ptrSrc, dst, dstIndex)
		if res.IsValid() {
			return res
		}
	case reflect.Ptr:
		for i := 0; i < src.NumMethod(); i++ {
			srcMethodValue := src.Method(i)

			if !srcMethodValue.IsValid() {
				continue
			}

			if src.Type().Method(i).Name != methodName {
				continue
			}

			result := srcMethodValue.Call([]reflect.Value{reflect.ValueOf(dc.args)})
			if len(result) < 1 {
				dc.SetError(errors.Errorf("`method` tag method:%s return empty", methodName))
				return dc.DefaultReflectValue()
			} else if len(result) > 1 {
				// 最后一个参数要求为error
				err, ok := result[len(result)-1].Interface().(error)
				if ok && err != nil {
					dc.SetError(err)
					return dc.DefaultReflectValue()
				}
			}

			return result[0]
		}
	}

	if dc.config.StrictMode {
		dc.SetError(errors.Errorf("couldn't find `method` tag method: %s", methodName))
		return dc.DefaultReflectValue()
	}

	return dc.NextSelector(src, dst, dstIndex)
}

// skip标签选择器
var SkipTagSelector = func(dc *DeepCopier, src, dst reflect.Value, dstIndex int) reflect.Value {
	_, ok := dc.curDstTags[SkipTagName]
	if ok {
		return dc.DefaultReflectValue()
	}

	return dc.NextSelector(src, dst, dstIndex)
}

// from标签选择器
var FromTagSelector = func(dc *DeepCopier, src, dst reflect.Value, dstIndex int) reflect.Value {
	if !src.IsValid() {
		return dc.NextSelector(src, dst, dstIndex)
	}

	fromName, ok := dc.curDstTags[FieldFromTagName]
	if !ok {
		return dc.NextSelector(src, dst, dstIndex)
	}

	switch src.Type().Kind() {
	case reflect.Ptr:
		res := dc.CurrentSelector(reflect.Indirect(src), dst, dstIndex)
		if res.IsValid() {
			return res
		}
	case reflect.Struct:
		for i := 0; i < src.NumField(); i++ {
			srcFieldValue := src.Field(i)
			srcFieldType := src.Type().Field(i)

			// 匿名结构体
			if srcFieldType.Anonymous {
				res := dc.CurrentSelector(srcFieldValue, dst, dstIndex)
				if res.IsValid() {
					return res
				}
			}

			if srcFieldType.Name == fromName {
				return srcFieldValue
			}
		}
	case reflect.Map:
		// key要求必须为string
		if src.Type().Key().Kind() != reflect.String {
			break
		}

		for _, i := range src.MapKeys() {
			if i.Interface().(string) == fromName && src.MapIndex(i).CanInterface() {
				// 为避免类型为interface，实际为其他类型，需二次反射
				return reflect.ValueOf(src.MapIndex(i).Interface())
			}
		}
	}

	if dc.config.StrictMode {
		dc.SetError(errors.Errorf("couldn't find `from` tag field: %s", fromName))
		return dc.DefaultReflectValue()
	}

	return dc.NextSelector(src, dst, dstIndex)
}

// 默认字段名选择器
var FieldNameSelector = func(dc *DeepCopier, src, dst reflect.Value, dstIndex int) reflect.Value {
	if !src.IsValid() {
		return dc.NextSelector(src, dst, dstIndex)
	}

	dstFieldType := dst.Type().Field(dstIndex)
	switch src.Type().Kind() {
	case reflect.Ptr:
		res := dc.CurrentSelector(reflect.Indirect(src), dst, dstIndex)
		if res.IsValid() {
			return res
		}
	case reflect.Struct:
		for i := 0; i < src.NumField(); i++ {
			srcFieldValue := src.Field(i)
			srcFieldType := src.Type().Field(i)

			// 匿名结构体
			if srcFieldType.Anonymous {
				res := dc.CurrentSelector(srcFieldValue, dst, dstIndex)
				if res.IsValid() {
					return res
				}
			}

			if srcFieldType.Name == dstFieldType.Name {
				return srcFieldValue
			}
		}
	case reflect.Map:
		// key要求必须为string
		if src.Type().Key().Kind() != reflect.String {
			break
		}

		for _, i := range src.MapKeys() {
			if i.Interface().(string) == dstFieldType.Name && src.MapIndex(i).CanInterface() {
				// 为避免类型为interface，实际为其他类型，需二次反射
				return reflect.ValueOf(src.MapIndex(i).Interface())
			}
		}
	}

	return dc.NextSelector(src, dst, dstIndex)
}
