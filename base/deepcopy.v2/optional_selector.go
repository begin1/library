package deepcopy

import (
	"reflect"
	"strings"
)

var ToTagSelector = func(dc *DeepCopier, src, dst reflect.Value, dstIndex int) reflect.Value {
	if !src.IsValid() {
		return dc.NextSelector(src, dst, dstIndex)
	}

	if !dc.IsEnableOptional(FieldToTagName) {
		return dc.NextSelector(src, dst, dstIndex)
	}

	dstTypeField := dst.Type().Field(dstIndex)
	switch src.Type().Kind() {
	case reflect.Ptr:
		res := dc.CurrentSelector(reflect.Indirect(src), dst, dstIndex)
		if res.IsValid() {
			return res
		}
	case reflect.Struct:
		for i := 0; i < src.NumField(); i++ {
			srcFieldValue := src.Field(i)
			srcFieldType := src.Type().Field(i)

			// 降低取tag频率
			if !strings.Contains(srcFieldType.Tag.Get(TagName), FieldToTagName) {
				continue
			}
			// todo:频繁取tag，会导致cpu过高
			tag := getTags(srcFieldType)
			toName, ok := tag[FieldToTagName]
			if !ok {
				continue
			}

			if srcFieldType.Anonymous {
				res := dc.CurrentSelector(srcFieldValue, dst, dstIndex)
				if res.IsValid() {
					return res
				}
			}

			if toName == dstTypeField.Name {
				return srcFieldValue
			}
		}
	}

	return dc.NextSelector(src, dst, dstIndex)
}
