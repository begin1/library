package sql

import (
	render "gitlab.com/begin1/library/base/logrender"
	"github.com/pkg/errors"
	// database driver
	_ "github.com/go-sql-driver/mysql"
)

// 新建mysql客户端
func NewMySQL(c *Config) (db *OrmDB) {
	if c == nil {
		panic("mysql config is nil")
	}

	if c.Config == nil {
		c.Config = &render.Config{}
	}

	db, err := OpenOrm(c)
	if err != nil {
		panic(errors.Wrap(err, "open mysql error"))
	}
	return
}
