package sql

import (
	"context"
	"fmt"
	_context "gitlab.com/begin1/library/base/context"
	"gitlab.com/begin1/library/base/runtime"
	"gitlab.com/begin1/library/net/metric"
	"gitlab.com/begin1/library/net/tracing"
	"github.com/jinzhu/gorm"
	"github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus"
	"strings"
	"time"
)

const (
	// 信息存储键
	SpanInfoStoreKey = "span_info"
	ContextStoreKey  = "scope_context"
)

// 自定义回调
type qtCallBack struct {
	// 日志logger
	logger *Logger
}

// 新建自定义回调
func newQTCallbacks(logger *Logger) *qtCallBack {
	return &qtCallBack{
		logger: logger,
	}
}

// 创建前回调
func (c *qtCallBack) beforeCreate(scope *gorm.Scope) { c.before(scope, "INSERT") }

// 创建后回调
func (c *qtCallBack) afterCreate(scope *gorm.Scope) { c.after(scope, "INSERT") }

// 查询前回调
func (c *qtCallBack) beforeQuery(scope *gorm.Scope) { c.before(scope, "SELECT") }

// 查询后回调
func (c *qtCallBack) afterQuery(scope *gorm.Scope) { c.after(scope, "SELECT") }

// 更新前回调
func (c *qtCallBack) beforeUpdate(scope *gorm.Scope) { c.before(scope, "UPDATE") }

// 更新后回调
func (c *qtCallBack) afterUpdate(scope *gorm.Scope) { c.after(scope, "UPDATE") }

// 删除前回调
func (c *qtCallBack) beforeDelete(scope *gorm.Scope) { c.before(scope, "DELETE") }

// 删除后回调
func (c *qtCallBack) afterDelete(scope *gorm.Scope) { c.after(scope, "DELETE") }

// 行数查询前回调
func (c *qtCallBack) beforeRowQuery(scope *gorm.Scope) {
	operation := strings.ToUpper(strings.Split(scope.SQL, " ")[0])
	c.before(scope, operation)
}

// 行数查询后回调
func (c *qtCallBack) afterRowQuery(scope *gorm.Scope) {
	operation := strings.ToUpper(strings.Split(scope.SQL, " ")[0])
	c.after(scope, operation)
}

type SpanInfo struct {
	StartTime time.Time
	EndTime   time.Time
	Duration  time.Duration

	Source       string
	Level        string
	DSNName      string
	Operation    string
	FullSql      string
	AffectedRows int

	TracingSpan opentracing.Span
	UUID        string
	WebUrl      string
	WebMethod   string
}

// 获取基本信息
func (c *qtCallBack) getSpanInfo(ctx context.Context, operation string) *SpanInfo {
	msg := new(SpanInfo)
	msg.UUID = _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown")
	msg.WebUrl = _context.GetString(ctx, _context.ContextRequestPathKey)
	msg.WebMethod = _context.GetString(ctx, _context.ContextRequestMethodKey)
	msg.Source = runtime.GetDefaultFilterCallers()
	msg.Level = "sql"
	msg.StartTime = time.Now()
	msg.DSNName = c.logger.DataSource
	msg.Operation = operation

	return msg
}

// 打印日志
func (c *qtCallBack) log(info *SpanInfo) {
	c.logger.Print(map[string]interface{}{
		"start_time": info.StartTime,
		"end_time":   info.EndTime,
		"uuid":       info.UUID,
		"source":     info.Source,
		"level":      info.Level,
		"duration":   info.Duration,
		"dsn":        info.DSNName,
		"sql":        info.FullSql,
		"rows":       info.AffectedRows,
	})
}

// 操作前统计
func (c *qtCallBack) metricBefore(info *SpanInfo) {
	metric.GormRequestTotal.With(
		prometheus.Labels{
			"web_url":    info.WebUrl,
			"web_method": info.WebMethod,
			"dsn":        info.DSNName,
			"operation":  info.Operation,
		},
	).Inc()
}

// 操作后统计
func (c *qtCallBack) metricAfter(info *SpanInfo) {
	metric.GormRequestDurationSummary.With(
		prometheus.Labels{
			"dsn":       info.DSNName,
			"operation": info.Operation,
		},
	).Observe(info.Duration.Seconds() * 1000)
}

// 操作前回调
func (c *qtCallBack) before(scope *gorm.Scope, operation string) {
	ctxValue, ok := scope.DB().Get(ContextStoreKey)
	if !ok {
		return
	}
	ctx := ctxValue.(context.Context)

	info := c.getSpanInfo(ctx, operation)
	scope.Set(SpanInfoStoreKey, info)
	c.metricBefore(info)
	ctx, span := tracing.GormTracingBefore(ctx, c.logger.DataSource, operation)
	scope.DB().Set(ContextStoreKey, ctx)
	info.TracingSpan = span
}

// 操作后回调
func (c *qtCallBack) after(scope *gorm.Scope, operation string) {
	infoValue, ok := scope.DB().Get(SpanInfoStoreKey)
	if !ok {
		return
	}
	info := infoValue.(*SpanInfo)

	info.EndTime = time.Now()
	info.Duration = info.EndTime.Sub(info.StartTime)
	info.AffectedRows = int(scope.DB().RowsAffected)
	info.FullSql = generateFullSQL(scope.SQL, scope.SQLVars)

	c.log(info)
	c.metricAfter(info)
	tracing.GormTracingAfter(info.TracingSpan, scope, info.FullSql, operation)
}

// 注册自定义回调
func RegisterCustomCallbacks(db *gorm.DB, logger *Logger) {
	callbacks := newQTCallbacks(logger)

	registerQTCallbacks(db, "create", callbacks)
	registerQTCallbacks(db, "query", callbacks)
	registerQTCallbacks(db, "update", callbacks)
	registerQTCallbacks(db, "delete", callbacks)
	registerQTCallbacks(db, "row_query", callbacks)
}

// 注册自定义回调
func registerQTCallbacks(db *gorm.DB, name string, c *qtCallBack) {
	beforeName := fmt.Sprintf("qt:%v_before", name)
	afterName := fmt.Sprintf("qt:%v_after", name)

	gormCallbackName := fmt.Sprintf("gorm:%v", name)

	switch name {
	case "create":
		db.Callback().Create().Before(gormCallbackName).Register(beforeName, c.beforeCreate)
		db.Callback().Create().After(gormCallbackName).Register(afterName, c.afterCreate)
	case "query":
		db.Callback().Query().Before(gormCallbackName).Register(beforeName, c.beforeQuery)
		db.Callback().Query().After(gormCallbackName).Register(afterName, c.afterQuery)
	case "update":
		db.Callback().Update().Before(gormCallbackName).Register(beforeName, c.beforeUpdate)
		db.Callback().Update().After(gormCallbackName).Register(afterName, c.afterUpdate)
	case "delete":
		db.Callback().Delete().Before(gormCallbackName).Register(beforeName, c.beforeDelete)
		db.Callback().Delete().After(gormCallbackName).Register(afterName, c.afterDelete)
	case "row_query":
		db.Callback().RowQuery().Before(gormCallbackName).Register(beforeName, c.beforeRowQuery)
		db.Callback().RowQuery().After(gormCallbackName).Register(afterName, c.afterRowQuery)
	}
}
