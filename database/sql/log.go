package sql

import (
	"fmt"
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const (
	_ormInfoFile = "gormInfo.log"

	defaultPattern = "%J{tsTUSG}"
)

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers    []LogWriter
	DataSource string
}

func (logger Logger) Print(m map[string]interface{}) {
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

// 获取默认writer
func getDefaultWriter(conf *Config, dataSource string) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _ormInfoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers:    writers,
		DataSource: dataSource,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"S": source,
	"s": startTime,
	"U": contextUUID,
	"t": title,
	"G": gormSQL,
	"D": dsn,
	"d": duration,
	"R": rows,
	"L": level,
	"F": fullSQL,
}

// 结束时间
func longTime(args map[string]interface{}) (string, interface{}) {
	return "time", args["end_time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 开始时间
func startTime(args map[string]interface{}) (string, interface{}) {
	return "start_time", args["start_time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "GORM"
}

// 日志打印的调用源
func source(args map[string]interface{}) (key string, value interface{}) {
	if _, ok := args["source"]; !ok {
		return "source", ""
	}
	return "source", args["source"]
}

// context中的uuid
func contextUUID(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args["uuid"].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 汇总的gorm参数
func gormSQL(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){dsn, duration, rows, level, fullSQL}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "gorm", m
}

// 数据源名称
func dsn(args map[string]interface{}) (key string, value interface{}) {
	if _, ok := args["dsn"]; !ok {
		return "dsn", ""
	}
	return "dsn", args["dsn"]
}

// 操作持续时间
func duration(args map[string]interface{}) (key string, value interface{}) {
	if _, ok := args["duration"]; !ok {
		return "duration", -1
	}
	originValue := float64(args["duration"].(time.Duration).Nanoseconds()/1e4) / 100.0
	duration, err := strconv.ParseFloat(fmt.Sprintf("%.2f", originValue), 64)
	if err != nil {
		duration = originValue
	}
	return "duration", duration
}

// 操作行数
func rows(args map[string]interface{}) (key string, value interface{}) {
	if _, ok := args["rows"]; !ok {
		return "rows", -1
	}
	return "rows", args["rows"].(int)
}

// gorm日志等级
func level(args map[string]interface{}) (key string, value interface{}) {
	if _, ok := args["level"]; !ok {
		return "level", ""
	}
	return "level", args["level"]
}

// 完整sql
func fullSQL(args map[string]interface{}) (key string, value interface{}) {
	if sql, ok := args["sql"]; !ok {
		return "sql", ""
	} else {
		return "sql", sql
	}
}
