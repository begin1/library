package mongo

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
	"reflect"
	"strings"
	"sync/atomic"
	"time"
)

// DB
type DB struct {
	// 主连接
	write *conn
	// 读连接数组
	read []*conn
	// 读索引号，用于分配读连接
	idx int64
	// 主数据库
	master *DB
	// 配置文件
	conf *Config

	// 当前集合名
	collectionName string
	// 当前连接
	currentConnect *conn
}

// 打开数据库
func Open(c *Config) (*DB, error) {
	db := new(DB)
	w, err := connect(c, c.DSN)
	if err != nil {
		return nil, err
	}

	if len(c.ReadDSN) == 0 {
		c.ReadDSN = []*DSNConfig{c.DSN}
	}
	rs := make([]*conn, 0, len(c.ReadDSN))
	for _, rd := range c.ReadDSN {
		r, err := connect(c, rd)
		if err != nil {
			return nil, err
		}
		rs = append(rs, r)
	}
	db.conf = c
	db.write = w
	db.read = rs
	db.master = &DB{write: db.write}

	return db, nil
}

// 获取配置文件
func (db *DB) GetConfig() *Config {
	return db.conf
}

func concatConnectURI(dsnConfig *DSNConfig) string {
	endpoints := make([]string, 0)
	for _, endpoint := range dsnConfig.Endpoints {
		endpoints = append(endpoints, fmt.Sprintf("%s:%d", endpoint.Address, endpoint.Port))
	}

	uri := fmt.Sprintf("mongodb://%s:%s@%s/%s",
		dsnConfig.UserName, dsnConfig.Password, strings.Join(endpoints, ","),
		dsnConfig.DBName)
	if len(dsnConfig.Options) != 0 {
		uri = fmt.Sprintf("%s?%s", uri, strings.Join(dsnConfig.Options, "&"))
	}

	return uri
}

// 进行数据库连接
func connect(c *Config, dsnConfig *DSNConfig) (*conn, error) {
	readPreference, err := readpref.New(readpref.SecondaryMode)
	if err != nil {
		return nil, err
	}
	opt := options.Client().ApplyURI(concatConnectURI(dsnConfig))
	opt.SetLocalThreshold(time.Duration(c.ExecTimeout))
	opt.SetMaxConnIdleTime(time.Duration(c.IdleTimeout))
	opt.SetMaxPoolSize(uint64(c.MaxPoolSize))
	opt.SetMinPoolSize(uint64(c.MinPoolSize))
	opt.SetReadPreference(readPreference)
	opt.SetWriteConcern(writeconcern.New(writeconcern.WMajority()))

	client, err := mongo.NewClient(opt)
	if err != nil {
		err = errors.WithStack(err)
		return nil, err
	}

	err = client.Connect(context.Background())
	if err != nil {
		err = errors.WithStack(err)
		return nil, err
	}
	return &conn{
		Client: client,
		conf:   c,
		dbName: dsnConfig.DBName,
		logger: getMongoDefaultWriter(c, dsnConfig.UserName),
	}, nil
}

// Ping操作
func (db *DB) Ping(c context.Context) (err error) {
	if err = db.write.ping(c, readpref.Primary()); err != nil {
		return
	}
	for _, rd := range db.read {
		if err = rd.ping(c, readpref.Secondary()); err != nil {
			return
		}
	}
	return
}

// 获取读连接索引
func (db *DB) readIndex() int {
	if len(db.read) == 0 {
		return 0
	}
	v := atomic.AddInt64(&db.idx, 1) % int64(len(db.read))
	atomic.StoreInt64(&db.idx, v)
	return int(v)
}

// 设置写连接的collection
func (db *DB) Collection(collectionName string) *DB {
	return &DB{
		conf:           db.conf,
		write:          db.write,
		read:           db.read,
		idx:            db.idx,
		master:         db.master,
		currentConnect: db.write,
		collectionName: collectionName,
	}
}

// 设置读连接的collection
func (db *DB) ReadOnlyCollection(collectionName string) *DB {
	return &DB{
		conf:           db.conf,
		write:          db.write,
		read:           db.read,
		idx:            db.idx,
		master:         db.master,
		currentConnect: db.read[db.readIndex()],
		collectionName: collectionName,
	}
}

// 关闭连接
func (db *DB) Close(c context.Context) (err error) {
	db.write.logger.Close()
	if e := db.write.Disconnect(c); e != nil {
		err = errors.WithStack(e)
	}
	for _, rd := range db.read {
		rd.logger.Close()
		if e := rd.Disconnect(c); e != nil {
			err = errors.WithStack(e)
		}
	}
	return
}

// 获取当前集合
func (db *DB) getCurrentCollection() (collection *mongo.Collection) {
	if db.currentConnect == nil || db.collectionName == "" {
		panic(errors.New("Current collection is nil"))
	} else {
		return db.currentConnect.Collection(db.collectionName)
	}
}

// 获取写context
func (db *DB) getExecContext(ctx context.Context) context.Context {
	_, execCtx, _ := db.conf.ExecTimeout.Shrink(ctx)
	return execCtx
}

// 获取读context
func (db *DB) getQueryContext(ctx context.Context) context.Context {
	_, queryCtx, _ := db.conf.QueryTimeout.Shrink(ctx)
	return queryCtx
}

// 批量写操作
func (db *DB) BulkWrite(ctx context.Context, models []mongo.WriteModel,
	opts ...*options.BulkWriteOptions) (*mongo.BulkWriteResult, error) {
	collection := db.getCurrentCollection()

	return collection.BulkWrite(db.getExecContext(ctx), models, opts...)
}

// 插入单个文档
func (db *DB) InsertOne(ctx context.Context, document interface{}, opts ...*options.InsertOneOptions) (result *mongo.InsertOneResult, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "InsertOne", db.collectionName, nil, document, nil, opts)
	result, err = collection.InsertOne(db.getExecContext(ctx), document, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 插入多个文档
func (db *DB) InsertMany(ctx context.Context, documents []interface{}, opts ...*options.InsertManyOptions) (result *mongo.InsertManyResult, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "InsertMany", db.collectionName, nil, documents, nil, opts)
	result, err = collection.InsertMany(db.getExecContext(ctx), documents, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 删除单个文档
func (db *DB) DeleteOne(ctx context.Context, filter interface{}, opts ...*options.DeleteOptions) (result *mongo.DeleteResult, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "DeleteOne", db.collectionName, filter, nil, nil, opts)
	result, err = collection.DeleteOne(db.getExecContext(ctx), filter, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 删除多个文档
func (db *DB) DeleteMany(ctx context.Context, filter interface{}, opts ...*options.DeleteOptions) (result *mongo.DeleteResult, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "DeleteMany", db.collectionName, filter, nil, nil, opts)
	result, err = collection.DeleteMany(db.getExecContext(ctx), filter, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 更新单个文档
func (db *DB) UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (result *mongo.UpdateResult, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "UpdateOne", db.collectionName, filter, update, nil, opts)
	result, err = collection.UpdateOne(db.getExecContext(ctx), filter, update, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 更新多个文档
func (db *DB) UpdateMany(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (result *mongo.UpdateResult, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "UpdateMany", db.collectionName, filter, update, nil, opts)
	result, err = collection.UpdateMany(db.getExecContext(ctx), filter, update, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 替换单个文档
func (db *DB) ReplaceOne(ctx context.Context, filter interface{}, replacement interface{}, opts ...*options.ReplaceOptions) (result *mongo.UpdateResult, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "ReplaceOne", db.collectionName, filter, replacement, nil, opts)
	result, err = collection.ReplaceOne(db.getExecContext(ctx), filter, replacement, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 聚合管道
func (db *DB) Aggregate(ctx context.Context, pipeline interface{}, opts ...*options.AggregateOptions) (result *FindResult) {
	collection := db.getCurrentCollection()
	execCtx := db.getExecContext(ctx)
	raws := make([]bson.Raw, 0)

	ctx, msg := db.currentConnect.before(ctx, "Aggregate", db.collectionName, nil, nil, pipeline, opts)
	cur, err := collection.Aggregate(execCtx, pipeline, opts...)
	db.currentConnect.after(msg, err)

	if err != nil {
		return &FindResult{
			raws: nil,
			err:  err,
		}
	}

	defer cur.Close(execCtx)
	for cur.Next(execCtx) {
		raws = append(raws, cur.Current)
	}

	return &FindResult{
		raws: raws,
		err:  nil,
	}
}

// 统计文档数
func (db *DB) CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (result int64, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "CountDocuments", db.collectionName, filter, nil, nil, opts)
	result, err = collection.CountDocuments(db.getQueryContext(ctx), filter, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 预估文档数，从info中直接获取
func (db *DB) EstimatedDocumentCount(ctx context.Context, opts ...*options.EstimatedDocumentCountOptions) (result int64, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "EstimatedDocumentCount", db.collectionName, nil, nil, nil, opts)
	result, err = collection.EstimatedDocumentCount(db.getQueryContext(ctx), opts...)
	db.currentConnect.after(msg, err)

	return
}

// 去重查找文档
func (db *DB) Distinct(ctx context.Context, fieldName string, filter interface{}, opts ...*options.DistinctOptions) (result []interface{}, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "Distinct", db.collectionName, filter, nil, fieldName, opts)
	result, err = collection.Distinct(db.getQueryContext(ctx), fieldName, filter, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 查找文档
func (db *DB) Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) (result *FindResult) {
	collection := db.getCurrentCollection()
	queryCtx := db.getQueryContext(ctx)
	raws := make([]bson.Raw, 0)

	ctx, msg := db.currentConnect.before(ctx, "Find", db.collectionName, filter, nil, nil, opts)
	cur, err := collection.Find(queryCtx, filter, opts...)
	db.currentConnect.after(msg, err)

	if err != nil {
		return &FindResult{
			raws: nil,
			err:  err,
		}
	}

	defer cur.Close(queryCtx)
	for cur.Next(queryCtx) {
		raws = append(raws, cur.Current)
	}

	return &FindResult{
		raws: raws,
		err:  nil,
	}
}

// 分页查找
func (db *DB) FindPage(ctx context.Context, filter interface{}, page, limit int, opts ...*options.FindOptions) (result *FindResult) {
	collection := db.getCurrentCollection()
	Limit := int64(limit)
	Skip := int64(page * limit)
	opts = append(opts, &options.FindOptions{
		Limit: &Limit,
		Skip:  &Skip,
	})

	queryCtx := db.getQueryContext(ctx)
	raws := make([]bson.Raw, 0)

	ctx, msg := db.currentConnect.before(ctx, "FindPage", db.collectionName, filter, nil, nil, opts)
	cur, err := collection.Find(ctx, filter, opts...)
	db.currentConnect.after(msg, err)

	if err != nil {
		return &FindResult{
			err: err,
		}
	}

	defer cur.Close(queryCtx)
	for cur.Next(queryCtx) {
		raws = append(raws, cur.Current)
	}

	return &FindResult{
		raws: raws,
		err:  nil,
	}
}

// 查找单个文档
func (db *DB) FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) *SingleResult {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "FindOne", db.collectionName, filter, nil, nil, opts)
	result := collection.FindOne(db.getQueryContext(ctx), filter, opts...)
	db.currentConnect.after(msg, result.Err())

	return &SingleResult{
		SingleResult: result,
	}
}

// 查找单个文档并删除
func (db *DB) FindOneAndDelete(ctx context.Context, filter interface{}, opts ...*options.FindOneAndDeleteOptions) *SingleResult {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "FindOneAndDelete", db.collectionName, filter, nil, nil, opts)
	result := collection.FindOneAndDelete(db.getExecContext(ctx), filter, opts...)
	db.currentConnect.after(msg, result.Err())

	return &SingleResult{
		SingleResult: result,
	}
}

// 查找单个文档并替换
func (db *DB) FindOneAndReplace(ctx context.Context, filter interface{}, replacement interface{}, opts ...*options.FindOneAndReplaceOptions) *SingleResult {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "FindOneAndReplace", db.collectionName, filter, replacement, nil, opts)
	result := collection.FindOneAndReplace(db.getExecContext(ctx), filter, replacement, opts...)
	db.currentConnect.after(msg, result.Err())

	return &SingleResult{
		SingleResult: result,
	}
}

// 查找单个文档并更新
func (db *DB) FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...*options.FindOneAndUpdateOptions) *SingleResult {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "FindOneAndUpdate", db.collectionName, filter, update, nil, opts)
	result := collection.FindOneAndUpdate(db.getExecContext(ctx), filter, update, opts...)
	db.currentConnect.after(msg, result.Err())

	return &SingleResult{
		SingleResult: result,
	}
}

// 观察数据库变动流
func (db *DB) Watch(ctx context.Context, pipeline interface{}, opts ...*options.ChangeStreamOptions) (result *mongo.ChangeStream, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "Watch", db.collectionName, nil, nil, pipeline, opts)
	result, err = collection.Watch(db.getQueryContext(ctx), pipeline, opts...)
	db.currentConnect.after(msg, err)

	return
}

// 获取索引
func (db *DB) Indexes(ctx context.Context) (result mongo.IndexView, err error) {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "Indexes", db.collectionName, nil, nil, nil, nil)
	result = collection.Indexes()
	db.currentConnect.after(msg, nil)

	return
}

// 丢弃
func (db *DB) Drop(ctx context.Context) error {
	collection := db.getCurrentCollection()

	ctx, msg := db.currentConnect.before(ctx, "Drop", db.collectionName, nil, nil, nil, nil)
	err := collection.Drop(db.getExecContext(ctx))
	db.currentConnect.after(msg, err)

	return err
}

// 获取mongo标签
func getMongoTags(field reflect.StructField) map[string]interface{} {
	tagString := field.Tag.Get("bson")

	tags := make(map[string]interface{})

	for idx, tag := range strings.Split(tagString, ",") {
		if idx == 0 && tag != "" {
			tags["column"] = tag
		} else {
			tags[tag] = true
		}
	}

	return tags
}

// 结构体转mongo可读取的map结构
// 如果需要转换空值，则转换的字段必须有convertible的标签
// todo:由于无法判断空值，所以只要包含convertible标签，都会转换
// Deprecated
// 	方法已废弃。如需更新零值，实体及request模型请使用 null包 中的对应类型
func StructToMongoMap(obj interface{}) map[string]interface{} {
	v := reflect.Indirect(reflect.ValueOf(obj))
	t := v.Type()

	var data = make(map[string]interface{})
	for i := 0; i < t.NumField(); i++ {
		var (
			vField = v.Field(i)
			tField = v.Type().Field(i)
			tagMap = getMongoTags(tField)
		)

		if tField.Type.Kind() == reflect.Struct && tField.Anonymous {
			childMap := StructToMongoMap(vField.Interface())
			for childKey, childValue := range childMap {
				data[childKey] = childValue
			}
			continue
		} else if _, ok := tagMap["convertible"]; vField.IsZero() && !ok {
			continue
		} else if columnName, ok := tagMap["column"]; ok {
			data[columnName.(string)] = vField.Interface()
		} else {
			data[tField.Name] = vField.Interface()
		}
	}
	return data
}
