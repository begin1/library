package mongo

import (
	"context"
	render "gitlab.com/begin1/library/base/logrender"
	"github.com/pkg/errors"
)

// 新建mongo客户端
func NewMongo(c *Config) (db *DB) {
	if c == nil {
		panic("mongo config is nil")
	}
	if c.DSN == nil {
		panic("mongo must be set dsn")
	}

	if c.Config == nil {
		c.Config = &render.Config{}
	}

	db, err := Open(c)
	if err != nil {
		panic(errors.Wrap(err, "open mongo error"))
	}

	err = db.Ping(context.Background())
	if err != nil {
		panic(errors.Wrap(err, "mongo health check error"))
	}

	return
}
