package mongo

import (
	"fmt"
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	reflectUtil "gitlab.com/begin1/library/base/reflect"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"time"
)

const (
	_infoFile = "mongoInfo.log"

	defaultPattern = "%J{tsTUSM}"
)

type logger interface {
	Print(m map[string]interface{})
	Close()
}

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
	// 数据源
	DataSource string
}

func (logger Logger) Print(m map[string]interface{}) {
	m["dsn"] = logger.DataSource
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Close() {
	for _, writer := range logger.writers {
		writer.Render.Close()
	}
}

// 获取默认writer
func getMongoDefaultWriter(conf *Config, dataSource string) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers:    writers,
		DataSource: dataSource,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"s": startTime,
	"S": source,
	"U": contextUUID,
	"t": title,
	"M": mongoExtra,
	"D": dsn,
	"d": duration,
	"N": dbName,
	"n": collectionName,
	"F": funcName,
	"f": filterField,
	"C": changeField,
	"E": extraField,
	"O": optionField,
}

// 结束时间
func longTime(args map[string]interface{}) (string, interface{}) {
	return "time", args["end_time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 开始时间
func startTime(args map[string]interface{}) (string, interface{}) {
	return "start_time", args["start_time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "MONGO"
}

// 日志打印的调用源
func source(args map[string]interface{}) (string, interface{}) {
	return "source", args["source"]
}

// context中的uuid
func contextUUID(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args["uuid"].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 汇总的mongo参数
func mongoExtra(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){dsn, duration, dbName, collectionName, funcName, filterField, changeField, extraField, optionField}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "mongo", m
}

// 数据源
func dsn(args map[string]interface{}) (string, interface{}) {
	return "dsn", args["dsn"]
}

// 操作持续时间
func duration(args map[string]interface{}) (string, interface{}) {
	originValue := float64(args["duration"].(time.Duration).Nanoseconds()/1e4) / 100.0
	duration, err := strconv.ParseFloat(fmt.Sprintf("%.2f", originValue), 64)
	if err != nil {
		duration = originValue
	}
	return "duration", duration
}

// db名称
func dbName(args map[string]interface{}) (string, interface{}) {
	return "db_name", args["db_name"]
}

// 集合名称
func collectionName(args map[string]interface{}) (string, interface{}) {
	return "collection_name", args["collection_name"]
}

// 调用函数名称
func funcName(args map[string]interface{}) (string, interface{}) {
	return "func_name", args["func_name"]
}

// 过滤的字段
func filterField(args map[string]interface{}) (string, interface{}) {
	return "filter_field", args["filter_field"]
}

// 改变的字段
func changeField(args map[string]interface{}) (string, interface{}) {
	return "change_field", args["change_field"]
}

// 额外的字段，如聚合管道
func extraField(args map[string]interface{}) (string, interface{}) {
	return "extra_field", args["extra_field"]
}

// 参数字段
func optionField(args map[string]interface{}) (string, interface{}) {
	return "option_field", getOptionsMap(args["option_field"])
}

// 反射转换interface为切片
func convertSlice(src interface{}) []interface{} {
	if src == nil {
		return make([]interface{}, 0)
	}

	s := reflect.ValueOf(src)
	if s.Kind() != reflect.Slice {
		panic("convertSlice() given a non-slice type")
	}

	dst := make([]interface{}, s.Len())
	for i := 0; i < s.Len(); i++ {
		dst[i] = s.Index(i).Interface()
	}
	return dst
}

// 获取option字段的map
func getOptionsMap(value interface{}) (m map[string]string) {
	slice, err := reflectUtil.InterfaceToSlice(value)
	if err != nil {
		return nil
	}

	m = make(map[string]string)
	for _, item := range slice {
		itemValue := reflect.ValueOf(item).Elem()
		itemType := reflect.TypeOf(item).Elem()
		for i := 0; i < itemValue.NumField(); i++ {
			if !itemValue.Field(i).IsNil() {
				m[itemType.Field(i).Name] = fmt.Sprintf("%#v", itemValue.Field(i).Elem().Interface())
			}
		}
	}
	return
}
