package mongo

import (
	"context"
	_context "gitlab.com/begin1/library/base/context"
	"gitlab.com/begin1/library/base/runtime"
	"gitlab.com/begin1/library/net/metric"
	"gitlab.com/begin1/library/net/tracing"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

// 连接
type conn struct {
	// mongo客户端
	*mongo.Client
	// 配置文件
	conf *Config
	// 日志logger
	logger logger
	// 数据库名称
	dbName string
}

// 设置集合
func (con *conn) Collection(collectionName string) *mongo.Collection {
	return con.Database(con.dbName).Collection(collectionName)
}

// Ping检查
func (con *conn) ping(c context.Context, rp *readpref.ReadPref) (err error) {
	_, c, cancel := con.conf.ExecTimeout.Shrink(c)
	err = con.Ping(c, rp)
	cancel()
	if err != nil {
		err = errors.WithStack(err)
	}
	return
}

type SpanInfo struct {
	StartTime time.Time
	EndTime   time.Time
	Duration  time.Duration

	Source         string
	FuncName       string
	CollectionName string
	DBName         string
	FilterField    interface{}
	ChangeField    interface{}
	ExtraField     interface{}
	OptionField    interface{}

	TracingSpan opentracing.Span
	UUID        string
	WebUrl      string
	WebMethod   string
}

// 打印日志
func (con *conn) log(msg *SpanInfo) {
	con.logger.Print(map[string]interface{}{
		"duration":        msg.Duration,
		"end_time":        msg.EndTime,
		"start_time":      msg.StartTime,
		"source":          msg.Source,
		"func_name":       msg.FuncName,
		"collection_name": msg.CollectionName,
		"db_name":         msg.DBName,
		"filter_field":    msg.FilterField,
		"change_field":    msg.ChangeField,
		"extra_field":     msg.ExtraField,
		"option_field":    msg.OptionField,
		"uuid":            msg.UUID,
	})
}

// 获取基本信息
func (con *conn) getSpanInfo(ctx context.Context, funcName, collectionName string, filterField, changeField, extraField, optionField interface{}) *SpanInfo {
	msg := new(SpanInfo)

	msg.StartTime = time.Now()
	msg.Source = runtime.GetDefaultFilterCallers()
	msg.FuncName = funcName
	msg.DBName = con.dbName
	msg.FuncName = funcName
	msg.CollectionName = collectionName
	msg.FilterField = filterField
	msg.ChangeField = changeField
	msg.ExtraField = extraField
	msg.OptionField = optionField
	msg.UUID = _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown")
	msg.WebUrl = _context.GetString(ctx, _context.ContextRequestPathKey)
	msg.WebMethod = _context.GetString(ctx, _context.ContextRequestMethodKey)

	return msg
}

// 操作前统计
func (con *conn) metricBefore(message *SpanInfo) {
	metric.MongoRequestTotal.With(
		prometheus.Labels{
			"web_url":         message.WebUrl,
			"web_method":      message.WebMethod,
			"func_name":       message.FuncName,
			"db_name":         message.DBName,
			"collection_name": message.CollectionName,
		},
	).Inc()
}

// 操作后统计
func (con *conn) metricAfter(message *SpanInfo) {
	metric.MongoRequestDurationSummary.With(
		prometheus.Labels{
			"func_name":       message.FuncName,
			"db_name":         message.DBName,
			"collection_name": message.CollectionName,
		},
	).Observe(message.Duration.Seconds() * 1000)
}

// 操作前注入
func (con *conn) before(ctx context.Context, funcName, collectionName string,
	filterField, changeField, extraField, optionField interface{}) (context.Context, *SpanInfo) {
	msg := con.getSpanInfo(ctx, funcName, collectionName, filterField, changeField, extraField, optionField)

	con.metricBefore(msg)
	ctx, span := tracing.MongoTracingBefore(ctx, msg.FuncName, msg.CollectionName)
	msg.TracingSpan = span

	return ctx, msg
}

// 操作后注入
func (con *conn) after(msg *SpanInfo, err error) {
	msg.EndTime = time.Now()
	msg.Duration = msg.EndTime.Sub(msg.StartTime)

	con.log(msg)
	con.metricAfter(msg)
	tracing.MongoTracingAfter(msg.TracingSpan, err)
}
