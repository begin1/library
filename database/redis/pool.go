package redis

import (
	"github.com/gomodule/redigo/redis"
)

// 连接池
type Pool struct {
	// 连接池
	*redis.Pool
	// 配置文件
	config *Config
	// 日志logger
	logger logger
}

// 获取连接
func (p *Pool) Get() *Conn {
	con := p.Pool.Get()

	return &Conn{
		Conn:   con,
		logger: p.logger,
		pool:   p,
	}
}

// 获取连接，执行命令，并关闭连接
func (p *Pool) WrapDo(doFunction func(con *Conn) error) error {
	con := p.Get()
	defer con.Close()

	return doFunction(con)
}

func (p *Pool) Close() (err error) {
	p.logger.Close()
	return
}
