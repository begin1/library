package redis

import (
	"fmt"
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const (
	_infoFile = "redisInfo.log"

	defaultPattern = "%J{tsTUSR}"
)

type logger interface {
	Print(m map[string]interface{})
	Close()
}

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
}

func (logger Logger) Print(m map[string]interface{}) {
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Close() {
	for _, writer := range logger.writers {
		writer.Render.Close()
	}
}

// 获取默认writer
func getDefaultWriter(conf *Config) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers: writers,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"E": endpoint,
	"T": longTime,
	"S": source,
	"s": startTime,
	"U": contextUUID,
	"t": title,
	"R": redisExtra,
	"D": duration,
	"N": funcName,
	"n": commandName,
	"a": commandArgs,
	"r": reply,
}

// Redis 连接地址
func endpoint(args map[string]interface{}) (string, interface{}) {
	return "endpoint", args["endpoint"]
}

// 结束时间
func longTime(args map[string]interface{}) (string, interface{}) {
	return "time", args["end_time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 开始时间
func startTime(args map[string]interface{}) (string, interface{}) {
	return "start_time", args["start_time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "REDIS"
}

// 日志打印的调用源
func source(args map[string]interface{}) (string, interface{}) {
	return "source", args["source"]
}

// context中的uuid
func contextUUID(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args["uuid"].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 汇总的redis参数
func redisExtra(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){endpoint, duration, funcName, commandName, commandArgs, reply}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "redis", m
}

// 操作持续时间
func duration(args map[string]interface{}) (string, interface{}) {
	originValue := float64(args["duration"].(time.Duration).Nanoseconds()/1e4) / 100.0
	duration, err := strconv.ParseFloat(fmt.Sprintf("%.2f", originValue), 64)
	if err != nil {
		duration = originValue
	}
	return "duration", duration
}

// 调用函数名
func funcName(args map[string]interface{}) (string, interface{}) {
	return "func_name", args["func_name"]
}

// 操作指令名
func commandName(args map[string]interface{}) (string, interface{}) {
	return "command_name", args["command_name"]
}

// 操作指令参数
func commandArgs(args map[string]interface{}) (string, interface{}) {
	return "command_args", args["command_args"]
}

// 响应
func reply(args map[string]interface{}) (string, interface{}) {
	return "reply", args["reply"]
}
