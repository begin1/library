package redis

import (
	"context"
	"fmt"
	_context "gitlab.com/begin1/library/base/context"
	"gitlab.com/begin1/library/base/runtime"
	"gitlab.com/begin1/library/net/metric"
	"gitlab.com/begin1/library/net/tracing"
	"github.com/gomodule/redigo/redis"
	"github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus"
	"time"
)

// 连接
type Conn struct {
	// 连接
	redis.Conn
	// 日志logger
	logger logger
	// 连接池
	pool *Pool
}

func (c *Conn) GetOriginConnect() redis.Conn {
	return c.Conn
}

// 执行命令
func (c *Conn) Do(ctx context.Context, commandName string, args ...interface{}) (reply interface{}, err error) {
	ctx, msg := c.before(ctx, "Do", commandName, args)

	reply, err = c.Conn.Do(commandName, args...)

	c.after(msg, err, reply)

	return
}

// 刷新输出缓存
func (c *Conn) Flush(ctx context.Context) error {
	ctx, msg := c.before(ctx, "Flush", "pipeline::flush", nil)

	err := c.Conn.Flush()

	c.after(msg, err, nil)

	return err
}

// 发送写命令
func (c *Conn) Send(ctx context.Context, commandName string, args ...interface{}) error {
	ctx, msg := c.before(ctx, "Send", fmt.Sprintf("pipeline::send::%s", commandName), args)

	err := c.Conn.Send(commandName, args...)

	c.after(msg, err, nil)

	return err
}

// 接受单个回复
func (c *Conn) Receive(ctx context.Context) (reply interface{}, err error) {
	ctx, msg := c.before(ctx, "Receive", "", nil)

	reply, err = c.Conn.Receive()

	c.after(msg, err, reply)

	return
}

// Ping操作
func (c *Conn) ping(ctx context.Context) (reply interface{}, err error) {
	reply, err = c.Conn.Do("PING")
	if err != nil {
		return nil, err
	}

	return reply, nil
}

type SpanInfo struct {
	Endpoint  string
	StartTime time.Time
	EndTime   time.Time
	Duration  time.Duration

	Source      string
	FuncName    string
	CommandName string
	CommandArgs []interface{}
	Reply       interface{}

	TracingSpan opentracing.Span
	UUID        string
	WebUrl      string
	WebMethod   string
}

// 打印日志
func (c *Conn) log(msg *SpanInfo) {
	c.logger.Print(map[string]interface{}{
		"endpoint":     msg.Endpoint,
		"start_time":   msg.StartTime,
		"end_time":     msg.EndTime,
		"duration":     msg.Duration,
		"source":       msg.Source,
		"func_name":    msg.FuncName,
		"command_name": msg.CommandName,
		"command_args": msg.CommandArgs,
		"replay":       msg.Reply,
		"uuid":         msg.UUID,
	})
}

// 获取基本信息
func (c *Conn) getSpanInfo(ctx context.Context, funcName, commandName string, commandArgs []interface{}) *SpanInfo {
	msg := new(SpanInfo)

	msg.Endpoint = c.pool.config.GetEndpoint()
	msg.StartTime = time.Now()
	msg.Source = runtime.GetDefaultFilterCallers()
	msg.FuncName = funcName
	msg.CommandName = commandName
	msg.CommandArgs = commandArgs
	msg.UUID = _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown")
	msg.WebUrl = _context.GetString(ctx, _context.ContextRequestPathKey)
	msg.WebMethod = _context.GetString(ctx, _context.ContextRequestMethodKey)

	return msg
}

// 操作前统计
func (c *Conn) metricBefore(message *SpanInfo) {
	metric.RedisRequestTotal.With(
		prometheus.Labels{
			"web_url":    message.WebUrl,
			"web_method": message.WebMethod,
			"endpoint":   message.Endpoint,
		},
	).Inc()
}

// 操作后统计
func (c *Conn) metricAfter(message *SpanInfo) {
	metric.RedisRequestDurationSummary.With(
		prometheus.Labels{
			"endpoint": message.Endpoint,
		},
	).Observe(message.Duration.Seconds() * 1000)
}

// 操作前注入
func (c *Conn) before(ctx context.Context, funcName, commandName string,
	commandArgs []interface{}) (context.Context, *SpanInfo) {
	msg := c.getSpanInfo(ctx, funcName, commandName, commandArgs)

	if msg.CommandName != "" {
		c.metricBefore(msg)
		ctx, span := tracing.RedisTracingBefore(ctx, msg.CommandName, msg.Endpoint)
		msg.TracingSpan = span
		return ctx, msg
	}

	return ctx, msg
}

// 操作后注入
func (c *Conn) after(msg *SpanInfo, err error, reply interface{}) {
	msg.EndTime = time.Now()
	msg.Duration = msg.EndTime.Sub(msg.StartTime)
	msg.Reply = reply

	c.log(msg)
	if msg.CommandName != "" {
		c.metricAfter(msg)
		tracing.RedisTracingAfter(msg.TracingSpan, err)
	}
}
