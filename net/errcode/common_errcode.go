package errcode

var (
	// 0-1999999 为保留错误码
	OK = add(0, "success")

	NotFound = add(1040404, "没有找到路由")

	InternalError      = add(1050500, "系统错误,请稍后重试")
	ServiceUnavailable = add(1050503, "服务暂不可用")

	UnknownError         = add(1060000, "未知错误")
	MysqlError           = add(1060001, "Mysql数据库错误")
	MongoError           = add(1060002, "Mongodb数据库错误")
	InvalidParams        = add(1060003, "参数错误")
	EncryptEncodeError   = add(1060004, "加密错误")
	EncryptDecodeError   = add(1060005, "解密错误")
	NoRowsFoundError     = add(1060006, "没有找到任何记录")
	RedisError           = add(1060007, "Redis数据库错误")
	RedisEmptyKeyError   = add(1060008, "Redis键为空")
	RabbitMQError        = add(1060009, "RabbitMQ错误")
	KafkaError           = add(1060010, "Kafka错误")
	RedLockError         = add(1060011, "RedLock错误")
	RedLockLockError     = add(1060012, "RedLock加锁错误")
	RedLockUnLockError   = add(1060013, "RedLock解锁错误")
	EtcdError            = add(1060014, "Etcd数据库错误")
	BreakerOpenError     = add(1060015, "断路器已开启")
	BreakerTimeoutError  = add(1060016, "断路器超时错误")
	BreakerDegradedError = add(1060017, "断路器已降级")
)
