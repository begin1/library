package errcode

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCause(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := NotFound
		result := Cause(e)
		assert.Equal(t, e.Code(), result.Code())
		assert.Equal(t, e.Message(), result.Message())
	})

	t.Run("empty", func(t *testing.T) {
		result := Cause(nil)
		assert.Equal(t, OK.Code(), result.Code())
		assert.Equal(t, OK.Message(), result.Message())
	})

	t.Run("new", func(t *testing.T) {
		e := errors.New("1040404:没有找到路由")
		result := Cause(e)
		assert.Equal(t, 1040404, result.Code())
		assert.Equal(t, "没有找到路由", result.Message())
	})
}

func TestEqualError(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := NotFound
		result := EqualError(NotFound, e)
		assert.Equal(t, true, result)
	})

	t.Run("new", func(t *testing.T) {
		e := errors.New("1040404:path no found")
		result := EqualError(NotFound, e)
		assert.Equal(t, true, result)
	})
}

func TestEqual(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := NotFound
		result := Equal(NotFound, e)
		assert.Equal(t, true, result)
	})

	t.Run("nil", func(t *testing.T) {
		result := Equal(OK, nil)
		assert.Equal(t, true, result)
	})
}

func TestGetErrorMessageMap(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		err := errors.Wrap(InternalError, "this is a test")

		result := GetErrorMessageMap(err)

		fmt.Printf("%#v\n", result)
	})
}

func BenchmarkGetErrorMessageMap(b *testing.B) {
	b.Run("normal", func(b *testing.B) {
		err := errors.Wrap(InternalError, "this is a test")
		b.ResetTimer()
		b.ReportAllocs()

		for i := 0; i < b.N; i++ {
			_ = GetErrorMessageMap(err)
		}
	})
}
