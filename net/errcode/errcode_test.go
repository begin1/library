package errcode

import (
	"errors"
	"fmt"
	pkgErrors "github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestString(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := NotFound
		result := String(fmt.Sprintf("%s", e))
		assert.Equal(t, e.Code(), result.Code())
		assert.Equal(t, e.Message(), result.Message())
	})
}

func TestErrCode_Error(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := NotFound
		result := fmt.Sprintf("%s", e)
		assert.Equal(t, "1040404:没有找到路由", result)
	})
}

func TestErrCode_WithMessage(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := NotFound
		f := NotFound.WithMessage("path found")
		assert.Equal(t, e.Code(), f.Code())
		assert.NotEqual(t, e.Message(), f.Message())
	})
}

func TestErrCode_FrontendCode(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := NotFound
		f := NotFound.WithFrontCode(2040404)
		assert.Equal(t, e.Message(), f.Message())
		assert.NotEqual(t, e.FrontendCode(), f.FrontendCode())
	})
}

func TestErrCode_Is(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := fmt.Errorf("current error is %w", UnknownError)

		assert.Equal(t, true, errors.Is(e, UnknownError))
		assert.Equal(t, true, errors.Is(e, pkgErrors.Wrapf(UnknownError, "%s", "wrap of ")))
	})
}

func TestErrCode_As(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		e := fmt.Errorf("current error is %w", UnknownError)
		target := new(ErrCode)

		assert.Equal(t, true, errors.As(e, target))
		assert.Equal(t, UnknownError.Code(), target.Code())
	})
}

func TestGetOrNewErrCode(t *testing.T) {
	t.Run("common", func(t *testing.T) {
		e := GetOrNewErrCode(0, "success")
		assert.Equal(t, 0, e.code)
		assert.Equal(t, 0, e.frontendCode)
		assert.Equal(t, "success", e.message)
	})

	t.Run("new", func(t *testing.T) {
		e := GetOrNewErrCode(9999999, "test")
		assert.Equal(t, 9999999, e.code)
		assert.Equal(t, 9999999, e.frontendCode)
		assert.Equal(t, "test", e.message)
	})

	t.Run("multi", func(t *testing.T) {
		e := GetOrNewErrCode(9999999, "test")
		assert.Equal(t, 9999999, e.code)
		assert.Equal(t, 9999999, e.frontendCode)
		assert.Equal(t, "test", e.message)
		_ = GetOrNewErrCode(9999999, "test")
	})
}
