package errcode

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

var (
	_codes = map[int]string{} // register codes.
)

// 新建业务错误码
// 业务错误码必须大于等于2000000
func New(e int, msg string) ErrCode {
	if e < 2000000 {
		panic("business ecode must greater than zero")
	}
	return add(e, msg)
}

// 添加错误码
func add(e int, msg string) ErrCode {
	if _, ok := _codes[e]; ok {
		panic(fmt.Sprintf("ecode: %d already exist", e))
	}
	_codes[e] = msg
	return ErrCode{
		code:         e,
		frontendCode: e,
		message:      msg,
	}
}

// 错误码
type ErrCode struct {
	// 基础错误码，要求唯一
	code int
	// 返回给前端的错误码
	frontendCode int
	// 错误信息
	message string
}

func (e ErrCode) Error() string {
	return fmt.Sprintf("%d:%s", e.code, e.message)
}

func (e ErrCode) Code() int { return e.code }

func (e ErrCode) Message() string {
	return e.message
}

func (e ErrCode) FrontendCode() int {
	return e.frontendCode
}

func (e ErrCode) Details() []interface{} {
	return []interface{}{
		e.code, e.frontendCode, e.message,
	}
}

// 设置前端错误码
func (e ErrCode) WithFrontCode(frontendCode int) ErrCode {
	e.frontendCode = frontendCode
	return e
}

// 设置错误信息
func (e ErrCode) WithMessage(message string) ErrCode {
	e.message = message
	return e
}

// 实现errors的Is接口
func (e ErrCode) Is(target error) bool {
	return EqualError(e, target)
}

// 实现errors的As接口
func (e ErrCode) As(target interface{}) bool {
	targetVal := reflect.ValueOf(target)
	if reflect.TypeOf(e).AssignableTo(targetVal.Type().Elem()) {
		targetVal.Elem().Set(reflect.ValueOf(e))
		return true
	}

	return false
}

// 判断是否是公共错误码
func IsCommonErrCode(code int) bool {
	if code >= 2000000 {
		return false
	}
	return true
}

// 通过code及message获取错误码
// 若存在，则返回
// 若不存在，则新建
func GetOrNewErrCode(code int, msg string) ErrCode {
	if IsCommonErrCode(code) {
		return String(fmt.Sprintf("%d:%s", code, msg))
	}
	return ErrCode{
		code:         code,
		frontendCode: code,
		message:      msg,
	}
}

// 从错误字符串中解析错误码
func String(e string) ErrCode {
	if e == "" {
		return OK
	}

	data := strings.Split(e, ":")
	if len(data) == 0 {
		return InternalError
	}

	code, err := strconv.Atoi(data[0])
	if err != nil {
		return InternalError
	}

	if _, ok := _codes[code]; !ok {
		panic(fmt.Sprintf("ecode: %d not exist", code))
	}

	return ErrCode{
		code:         code,
		frontendCode: code,
		message:      _codes[code],
	}
}
