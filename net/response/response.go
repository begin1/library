package response

import (
	"context"
	"errors"
	_context "gitlab.com/begin1/library/base/context"
	"gitlab.com/begin1/library/log"
	"gitlab.com/begin1/library/net/errcode"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/render"
	_errors "github.com/pkg/errors"
	"net/http"
)

// 基础响应
type Response struct {
	Code    int         `json:"errcode"`
	Message string      `json:"errmsg"`
	Data    interface{} `json:"data,omitempty"`
}

func (r *Response) WithErrCode(code errcode.Codes) interface{} {
	return Response{
		Code:    code.FrontendCode(),
		Message: code.Message(),
		Data:    r.Data,
	}
}

func (r *Response) PrintError(ctx context.Context, err error) {
	log.Errorv(ctx, errcode.GetErrorMessageMap(err))
}

func (r *Response) GetStatusCode(code errcode.Codes) int {
	switch code.Code() {
	case errcode.NotFound.Code():
		return http.StatusNotFound
	case errcode.ServiceUnavailable.Code():
		return http.StatusServiceUnavailable
	default:
		return http.StatusOK
	}
}

// 带错误码的响应接口
type ErrCodeResponse interface {
	PrintError(ctx context.Context, err error)

	WithErrCode(code errcode.Codes) interface{}

	GetStatusCode(code errcode.Codes) int
}

// 自定义返回json
// 自定义结构体需要实现ErrCodeResponse接口
func InjectJson(ctx *gin.Context, data ErrCodeResponse, err error) {
	if err != nil {
		data.PrintError(ctx, err)
	}

	// 防止超时请求
	select {
	case <-ctx.Request.Context().Done():
		return
	default:
	}

	// 适配fmt.Errorf及errors.Wrapf方法
	var code errcode.Codes
	if !errors.As(err, &code) {
		code = errcode.Cause(err)
	}
	ctx.Set(_context.ContextErrCode, code.Code())

	statusCode := data.GetStatusCode(code)

	// 渲染
	ctx.Render(statusCode, render.JSON{
		Data: data.WithErrCode(code),
	})
}

// 返回json
func JSON(ctx *gin.Context, data interface{}, err error) {
	InjectJson(
		ctx,
		&Response{
			Data: data,
		},
		err,
	)
}

// 返回非法请求参数的json
func InvalidParamsJSON(ctx *gin.Context, data interface{}, err error) {
	JSON(ctx, data, _errors.Wrap(errcode.InvalidParams, err.Error()))
}
