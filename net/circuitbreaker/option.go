package circuitbreaker

import (
	"gitlab.com/begin1/library/base/sw"
	"github.com/cenkalti/backoff"
	"time"
)

// 配置
type Options struct {
	// 退避相关函数
	BackOff backoff.BackOff
	// 触发断路函数
	ShouldTrip TripFunc
	// 滑动窗口滑动间隔
	windowSlideInterval time.Duration
	// 滑动窗口覆盖的桶数量
	windowBucketCount int
}

// 通过配置新建断路器
func NewBreakerWithOptions(options *Options) *Breaker {
	if options == nil {
		options = &Options{}
	}

	if options.BackOff == nil {
		options.BackOff = NewExponentialBackOff(time.Millisecond*500, 0)
	}

	if options.windowSlideInterval == 0 {
		options.windowSlideInterval = time.Second
	}

	if options.windowBucketCount == 0 {
		options.windowBucketCount = 10
	}

	return &Breaker{
		backOff:     options.BackOff,
		ShouldTrip:  options.ShouldTrip,
		nextBackOff: options.BackOff.NextBackOff(),
		counts:      sw.NewSlidingWindow(options.windowSlideInterval, options.windowBucketCount),
	}
}

// 新建断路器
func NewBreaker() *Breaker {
	return NewBreakerWithOptions(nil)
}

// 新建阀值断路器
func NewThresholdBreaker(threshold int64) *Breaker {
	return NewBreakerWithOptions(&Options{
		ShouldTrip: ThresholdTripFunc(threshold),
	})
}

// 新建连续断路器
func NewConsecutiveBreaker(threshold int64) *Breaker {
	return NewBreakerWithOptions(&Options{
		ShouldTrip: ConsecutiveTripFunc(threshold),
	})
}

// 新建比例断路器
func NewRateBreaker(rate float64, minSamples int64) *Breaker {
	return NewBreakerWithOptions(&Options{
		ShouldTrip: RateTripFunc(rate, minSamples),
	})
}

// 新建指数退避
func NewExponentialBackOff(interval, maxElapsedTime time.Duration) backoff.BackOff {
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = interval
	b.MaxElapsedTime = maxElapsedTime
	b.Reset()
	return b
}
