package circuitbreaker

import (
	"context"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestBreaker_Trip(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		cb := NewBreaker()
		assert.Equal(t, false, cb.IsTripped())

		cb.Trip()
		assert.Equal(t, true, cb.IsTripped())

		cb.Reset()
		assert.Equal(t, false, cb.IsTripped())
	})
}

func TestBreakerCounts(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		cb := NewBreaker()

		cb.Fail()
		assert.Equal(t, int64(1), cb.FailureCount())

		cb.Fail()
		assert.Equal(t, int64(2), cb.ConsecutiveFailureCount())

		cb.Success()
		assert.Equal(t, int64(1), cb.SuccessCount())
		assert.Equal(t, int64(0), cb.ConsecutiveFailureCount())

		cb.Reset()
		assert.Equal(t, int64(0), cb.SuccessCount())
		assert.Equal(t, int64(0), cb.ConsecutiveFailureCount())
		assert.Equal(t, int64(0), cb.FailureCount())
	})
}

func TestBreaker_Ready(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		cb := NewBreakerWithOptions(&Options{
			BackOff: NewExponentialBackOff(time.Millisecond*500, 0),
		})
		assert.Equal(t, true, cb.Ready())

		cb.Trip()
		assert.Equal(t, false, cb.Ready())

		time.Sleep(cb.nextBackOff + 1)
		assert.Equal(t, true, cb.Ready())

		cb.Fail()
		time.Sleep(cb.nextBackOff + 1)
		assert.Equal(t, true, cb.Ready())
	})
}

func TestBreaker_Break(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		cb := NewBreaker()

		cb.Break()
		time.Sleep(cb.nextBackOff + 1)
		assert.Equal(t, false, cb.Ready())

		cb.Reset()
		cb.Trip()
		time.Sleep(cb.nextBackOff + 1)
		assert.Equal(t, true, cb.Ready())
	})
}

func TestBreaker_Call(t *testing.T) {
	f := func() error {
		return errors.New("this is a test")
	}

	t.Run("normal", func(t *testing.T) {
		cb := NewThresholdBreaker(2)
		ctx, cancel := context.WithCancel(context.Background())

		err := cb.Call(ctx, f, 0)
		assert.NotNil(t, err)
		assert.Equal(t, false, cb.IsTripped())
		cancel()
		err = cb.Call(ctx, f, 0)
		assert.NotNil(t, err)
		assert.Equal(t, false, cb.IsTripped())

		err = cb.Call(context.Background(), f, 0)
		assert.NotNil(t, err)
		assert.Equal(t, true, cb.IsTripped())
	})
}
