package circuitbreaker

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewThresholdBreaker(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		cb := NewThresholdBreaker(2)
		assert.Equal(t, false, cb.IsTripped())

		cb.Fail()
		assert.Equal(t, false, cb.IsTripped())

		cb.Fail()
		assert.Equal(t, true, cb.IsTripped())

		cb.Reset()
		assert.Equal(t, int64(0), cb.FailureCount())
		assert.Equal(t, false, cb.IsTripped())
	})
}

func TestNewConsecutiveBreaker(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		cb := NewConsecutiveBreaker(3)
		assert.Equal(t, false, cb.IsTripped())

		cb.Fail()
		cb.Success()
		cb.Fail()
		cb.Fail()
		assert.Equal(t, false, cb.IsTripped())

		cb.Fail()
		assert.Equal(t, true, cb.IsTripped())
	})
}

func TestNewRateBreaker(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		cb := NewRateBreaker(0.5, 4)
		cb.Success()
		cb.Success()
		cb.Fail()
		cb.Fail()

		assert.Equal(t, true, cb.IsTripped())
		assert.Equal(t, 0.5, cb.ErrorRate())
	})

	t.Run("sample", func(t *testing.T) {
		cb := NewRateBreaker(0.5, 100)
		cb.Fail()

		assert.Equal(t, false, cb.IsTripped())
	})
}
