package circuitbreaker

import (
	"context"
	"gitlab.com/begin1/library/base/sw"
	"gitlab.com/begin1/library/net/errcode"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cenkalti/backoff"
)

// 断路器状态
type state int

const (
	// 开
	open state = 0
	// 半开
	halfOpen state = 1
	// 关
	closed state = 2
)

// 断路器
type Breaker struct {
	// 退避相关函数
	backOff backoff.BackOff
	// 下次退避时间
	nextBackOff time.Duration
	// 用于退避的锁
	backOffMutex sync.Mutex

	// 触发断路函数
	ShouldTrip TripFunc
	// 连续失败次数
	consecutiveFailuresCount int64
	// 上次失败时间
	lastFailureTime int64
	// 是否半开
	halfOpens int64
	// 计数桶滑动窗口
	counts *sw.SlidingWindow
	// 是否断路
	tripped int32
	// 是否终止
	broken int32

	// 修复599问题
	// https://github.com/golang/go/issues/599
	_ [4]byte
}

// 断路
func (cb *Breaker) Trip() {
	atomic.StoreInt32(&cb.tripped, 1)
	atomic.StoreInt64(&cb.lastFailureTime, time.Now().UnixNano())
}

// 重置
func (cb *Breaker) Reset() {
	atomic.StoreInt32(&cb.broken, 0)
	atomic.StoreInt32(&cb.tripped, 0)
	atomic.StoreInt64(&cb.halfOpens, 0)
	cb.ResetCounters()
}

// 重置计数器
func (cb *Breaker) ResetCounters() {
	atomic.StoreInt64(&cb.consecutiveFailuresCount, 0)
	cb.counts.Reset()
}

// 终止断路器
func (cb *Breaker) Break() {
	atomic.StoreInt32(&cb.broken, 1)
	cb.Trip()
}

// 单次失败
func (cb *Breaker) Fail() {
	cb.counts.Fail()

	atomic.AddInt64(&cb.consecutiveFailuresCount, 1)
	atomic.StoreInt64(&cb.lastFailureTime, time.Now().UnixNano())

	if cb.ShouldTrip != nil && cb.ShouldTrip(cb) {
		cb.Trip()
	}
}

// 单次成功
func (cb *Breaker) Success() {
	cb.backOffMutex.Lock()
	cb.backOff.Reset()
	cb.nextBackOff = cb.backOff.NextBackOff()
	cb.backOffMutex.Unlock()

	state := cb.state()
	if state == halfOpen {
		cb.Reset()
	}
	atomic.StoreInt64(&cb.consecutiveFailuresCount, 0)

	cb.counts.Success()
}

// 是否准备好，即是否允许真实调用
func (cb *Breaker) Ready() bool {
	state := cb.state()
	if state == halfOpen {
		atomic.StoreInt64(&cb.halfOpens, 0)
	}

	return state == closed || state == halfOpen
}

// 调用函数
// 当timeout为0时，表示无超时
func (cb *Breaker) Call(ctx context.Context, f func() error, timeout time.Duration) error {
	var err error

	if !cb.Ready() {
		return errcode.BreakerOpenError
	}

	if timeout == 0 {
		err = f()
	} else {
		c := make(chan error, 1)
		go func() {
			c <- f()
			close(c)
		}()

		select {
		case e := <-c:
			err = e
		case <-time.After(timeout):
			err = errcode.BreakerTimeoutError
		}
	}

	if err != nil {
		if ctx.Err() != context.Canceled {
			cb.Fail()
		}
		return err
	}
	cb.Success()

	return nil
}

// 获取状态
func (cb *Breaker) state() state {
	tripped := cb.IsTripped()
	if !tripped {
		return closed
	}

	if atomic.LoadInt32(&cb.broken) == 1 {
		return open
	}

	since := time.Now().Sub(time.Unix(0, atomic.LoadInt64(&cb.lastFailureTime)))

	cb.backOffMutex.Lock()
	defer cb.backOffMutex.Unlock()

	// 允许重试
	if cb.nextBackOff != backoff.Stop && since > cb.nextBackOff {
		if atomic.CompareAndSwapInt64(&cb.halfOpens, 0, 1) {
			cb.nextBackOff = cb.backOff.NextBackOff()
			return halfOpen
		}

		return open
	}

	return open
}

// 是否断路
func (cb *Breaker) IsTripped() bool {
	return atomic.LoadInt32(&cb.tripped) == 1
}

// 失败次数
func (cb *Breaker) FailureCount() int64 {
	return cb.counts.FailureCount()
}

// 连续失败次数
func (cb *Breaker) ConsecutiveFailureCount() int64 {
	return atomic.LoadInt64(&cb.consecutiveFailuresCount)
}

// 成功次数
func (cb *Breaker) SuccessCount() int64 {
	return cb.counts.SuccessCount()
}

// 错误比例
func (cb *Breaker) ErrorRate() float64 {
	return cb.counts.ErrorRate()
}
