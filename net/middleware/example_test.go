package middleware

import (
	"gitlab.com/begin1/library/base/ctime"
	"github.com/gin-gonic/gin"
	"time"
)

func ExampleMiddleware() {
	router := gin.New()

	router.Use(ParseUserAgentMiddleware())

	router.Use(GetUUIDMiddleware("QT-User-Id"))

	router.Use(CatchPanicMiddleware())

	router.Use(TimeoutMiddleware(ctime.Duration(time.Second)))
}
