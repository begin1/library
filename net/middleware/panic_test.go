package middleware

import (
	httpUtil "gitlab.com/begin1/library/base/net"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestCatchPanicMiddleware(t *testing.T) {
	t.Run("error", func(t *testing.T) {
		router := gin.New()
		router.Use(CatchPanicMiddleware())
		router.GET("/", func(c *gin.Context) {
			panic(errors.New("this is a panic"))
		})

		r, err := httpUtil.TestGinJsonRequest(router, "GET", "/", nil, nil, nil)
		assert.Nil(t, err)
		assert.Equal(t, http.StatusOK, r.Code)
	})

	t.Run("string", func(t *testing.T) {
		router := gin.New()
		router.Use(CatchPanicMiddleware())
		router.GET("/", func(c *gin.Context) {
			panic("this is a panic")
		})

		r, err := httpUtil.TestGinJsonRequest(router, "GET", "/", nil, nil, nil)
		assert.Nil(t, err)
		assert.Equal(t, http.StatusOK, r.Code)
	})
}
