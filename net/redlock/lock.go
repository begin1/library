package redlock

import (
	render "gitlab.com/begin1/library/base/logrender"
	"gitlab.com/begin1/library/database/redis"
	"github.com/go-redsync/redsync"
	"time"
)

// 客户端
type RedLock struct {
	// 管理工具
	*redsync.Redsync
	// 配置文件
	config *Config
}

// 新建客户端
func New(config *Config, pools ...*redis.Pool) *RedLock {
	if config.Config == nil {
		config.Config = &render.Config{}
	}

	redPool := make([]redsync.Pool, 0)
	for _, r := range pools {
		redPool = append(redPool, &Pool{
			Pool: r,
		})
	}

	return &RedLock{
		Redsync: redsync.New(redPool),
		config:  config,
	}
}

// 新建分布式锁
func (r *RedLock) NewMutex(name string, options ...redsync.Option) *Mutex {
	options = append(r.getConfigOptions(), options...)

	return &Mutex{
		name:   name,
		Mutex:  r.Redsync.NewMutex(name, options...),
		logger: getDefaultWriter(r.config),
	}
}

// 获取配置文件中的锁配置
func (r *RedLock) getConfigOptions() []redsync.Option {
	var options []redsync.Option

	if r.config.ExpiryTime != 0 {
		options = append(options, redsync.SetExpiry(time.Duration(r.config.ExpiryTime)))
	}

	if r.config.Tries != 0 {
		options = append(options, redsync.SetTries(r.config.Tries))
	}

	if r.config.RetryDelay != 0 {
		options = append(options, redsync.SetRetryDelay(time.Duration(r.config.RetryDelay)))
	}

	if r.config.DriftFactor != 0 {
		options = append(options, redsync.SetDriftFactor(r.config.DriftFactor))
	}

	return options
}
