package redlock

import (
	"context"
	_context "gitlab.com/begin1/library/base/context"
	"gitlab.com/begin1/library/base/runtime"
	"gitlab.com/begin1/library/net/metric"
	"gitlab.com/begin1/library/net/tracing"
	"github.com/go-redsync/redsync"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"time"
)

const (
	StateSuccess = "success"
	StateFail    = "fail"
)

// 分布式锁
type Mutex struct {
	// 分布式锁
	*redsync.Mutex
	// 日志logger
	logger logger
	// key名
	name string
}

// 锁
func (m *Mutex) Lock(ctx context.Context) error {
	ctx, info := m.before(ctx, "LOCK")
	e := m.Mutex.Lock()
	m.after(info, e)

	return e
}

// 解锁
func (m *Mutex) Unlock(ctx context.Context) bool {
	ctx, info := m.before(ctx, "UNLOCK")
	result := m.Mutex.Unlock()
	var err error
	if !result {
		err = errors.New("Unlock Fail")
	}
	m.after(info, err)

	return result
}

// 延长锁的时间
func (m *Mutex) Extend(ctx context.Context) bool {
	ctx, info := m.before(ctx, "EXTEND")
	result := m.Mutex.Extend()
	var err error
	if !result {
		err = errors.New("Extend Fail")
	}
	m.after(info, err)

	return result
}

type SpanInfo struct {
	StartTime time.Time
	EndTime   time.Time
	Duration  time.Duration

	Source      string
	MutexName   string
	CommandName string
	State       string

	TracingSpan opentracing.Span
	UUID        string
	WebUrl      string
	WebMethod   string
}

// 打印日志
func (m *Mutex) log(msg *SpanInfo) {
	m.logger.Print(map[string]interface{}{
		"start_time":   msg.StartTime,
		"end_time":     msg.EndTime,
		"duration":     msg.Duration,
		"source":       msg.Source,
		"mutex_name":   msg.MutexName,
		"command_name": msg.CommandName,
		"state":        msg.State,
		"uuid":         msg.UUID,
	})
}

// 获取基本信息
func (m *Mutex) getSpanInfo(ctx context.Context, commandName string) *SpanInfo {
	msg := new(SpanInfo)

	msg.StartTime = time.Now()
	msg.Source = runtime.GetDefaultFilterCallers()
	msg.CommandName = commandName
	msg.MutexName = m.name
	msg.UUID = _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown")
	msg.WebUrl = _context.GetString(ctx, _context.ContextRequestPathKey)
	msg.WebMethod = _context.GetString(ctx, _context.ContextRequestMethodKey)

	return msg
}

// 操作前统计
func (m *Mutex) metricBefore(message *SpanInfo) {
	metric.RedlockRequestTotal.With(
		prometheus.Labels{
			"web_url":    message.WebUrl,
			"web_method": message.WebMethod,
		},
	).Inc()
}

// 操作后统计
func (m *Mutex) metricAfter(message *SpanInfo) {
}

// 操作前注入
func (m *Mutex) before(ctx context.Context, commandName string) (context.Context, *SpanInfo) {
	msg := m.getSpanInfo(ctx, commandName)

	m.metricBefore(msg)
	ctx, span := tracing.RedlockTracingBefore(ctx, msg.MutexName, msg.CommandName)
	msg.TracingSpan = span

	return ctx, msg
}

// 操作后注入
func (m *Mutex) after(msg *SpanInfo, err error) {
	msg.EndTime = time.Now()
	msg.Duration = msg.EndTime.Sub(msg.StartTime)
	if err != nil {
		msg.State = StateFail
	} else {
		msg.State = StateSuccess
	}

	m.log(msg)
	m.metricAfter(msg)
	tracing.RedlockTracingAfter(msg.TracingSpan, err)
}
