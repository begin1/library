package httpclient

import "net/url"

// 请求Form
type Form struct {
	url.Values
}

// 新建Form参数
func NewForm(key, value string) *Form {
	v := url.Values{}
	v.Set(key, value)

	return &Form{
		Values: v,
	}
}

// 添加Form参数
func (f *Form) Add(key, value string) *Form {
	f.Set(key, value)
	return f
}
