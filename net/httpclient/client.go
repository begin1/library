package httpclient

import (
	"context"
	"gitlab.com/begin1/library/base/ctime"
	"gitlab.com/begin1/library/net/circuitbreaker"
	"github.com/pkg/errors"
	"net"
	"net/http"
	"time"
)

// 客户端
type Client struct {
	// 配置文件
	conf *Config
	// 日志logger
	logger logger
	// 断路器组
	breakerGroup *circuitbreaker.BreakerGroup
	// 负载均衡器
	loadBalancer *LoadBalancer
	// 全局上下文
	globalContext context.Context
	// 全局上下文取消函数
	globalCancelFunc context.CancelFunc
	// 实际客户端
	client *http.Client
}

// 打开http客户端
func Open(c *Config) (*Client, error) {
	if c.MaxIdleConns == 0 {
		c.MaxIdleConns = DefaultMaxIdleConns
	}
	if c.MaxIdleConnsPerHost == 0 {
		c.MaxIdleConnsPerHost = DefaultMaxIdleConnsPerHost
	}
	if c.IdleConnTimeout == 0 {
		c.IdleConnTimeout = ctime.Duration(DefaultIdleConnTimeout)
	}

	client := new(Client)
	client.conf = c
	client.logger = getDefaultWriter(c)
	client.breakerGroup = circuitbreaker.NewBreakerGroup()
	client.globalContext, client.globalCancelFunc = context.WithCancel(context.Background())
	client.client = &http.Client{
		Timeout: time.Duration(c.RequestTimeout),
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			DisableKeepAlives:     c.DisableKeepAlives,
			MaxIdleConns:          c.MaxIdleConns,
			MaxIdleConnsPerHost:   c.MaxIdleConnsPerHost,
			MaxConnsPerHost:       c.MaxConnsPerHost,
			IdleConnTimeout:       time.Duration(c.IdleConnTimeout),
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}

	if client.conf.EnableLoadBalancer {
		balancer, err := NewLoadBalancer(client.globalContext, client.logger)
		if err != nil {
			return nil, errors.Wrap(err, "http client init load balancer error")
		}
		client.loadBalancer = balancer
	}

	return client, nil
}

// 创建构建器
func (client *Client) Builder() *Span {
	return NewSpan(client)
}

// Deprecated: 推荐使用 Builder() 方法
// 请求form
func (client *Client) fetchForm(ctx context.Context, method, url string, queryParams *UrlValue, headers *Header, requestForm *Form) (resp *Response) {
	if headers == nil {
		headers = NewFormURLEncodedHeader()
	}

	return client.Builder().
		Method(method).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		FormBody(requestForm).
		Fetch(ctx)
}

// Deprecated: 推荐使用 Builder() 方法
// 请求json
func (client *Client) fetchJSON(ctx context.Context, method, url string, queryParams *UrlValue, headers *Header, requestBody interface{}) (resp *Response) {
	if headers == nil {
		headers = NewJsonHeader()
	}

	return client.Builder().
		Method(method).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		JsonBody(requestBody).
		Fetch(ctx)
}

// Deprecated: 推荐使用 Builder() 方法
// Post请求Form
func (client *Client) PostForm(ctx context.Context, url string, queryParams *UrlValue, requestForm *Form, headers *Header) (resp *Response) {
	return client.fetchForm(ctx, http.MethodPost, url, queryParams, headers, requestForm)
}

// Deprecated: 推荐使用 Builder() 方法
// Patch请求Form
func (client *Client) PatchForm(ctx context.Context, url string, queryParams *UrlValue, requestForm *Form, headers *Header) (resp *Response) {
	return client.fetchForm(ctx, http.MethodPatch, url, queryParams, headers, requestForm)
}

// Deprecated: 推荐使用 Builder() 方法
// Put请求Form
func (client *Client) PutForm(ctx context.Context, url string, queryParams *UrlValue, requestForm *Form, headers *Header) (resp *Response) {
	return client.fetchForm(ctx, http.MethodPut, url, queryParams, headers, requestForm)
}

// Deprecated: 推荐使用 Builder() 方法
// Get请求json
func (client *Client) GetJSON(ctx context.Context, url string, queryParams *UrlValue, headers *Header) (resp *Response) {
	return client.fetchJSON(ctx, http.MethodGet, url, queryParams, headers, nil)
}

// Deprecated: 推荐使用 Builder() 方法
// Post请求json
func (client *Client) PostJSON(ctx context.Context, url string, queryParams *UrlValue, body interface{}, headers *Header) (resp *Response) {
	return client.fetchJSON(ctx, http.MethodPost, url, queryParams, headers, body)
}

// Deprecated: 推荐使用 Builder() 方法
// Patch请求json
func (client *Client) PatchJSON(ctx context.Context, url string, queryParams *UrlValue, body interface{}, headers *Header) (resp *Response) {
	return client.fetchJSON(ctx, http.MethodPatch, url, queryParams, headers, body)
}

// Deprecated: 推荐使用 Builder() 方法
// Put请求json
func (client *Client) PutJSON(ctx context.Context, url string, queryParams *UrlValue, body interface{}, headers *Header) (resp *Response) {
	return client.fetchJSON(ctx, http.MethodPut, url, queryParams, headers, body)
}

// Deprecated: 推荐使用 Builder() 方法
// Delete请求json
func (client *Client) DeleteJSON(ctx context.Context, url string, queryParams *UrlValue, headers *Header) (resp *Response) {
	return client.fetchJSON(ctx, http.MethodDelete, url, queryParams, headers, nil)
}

// Deprecated: 推荐使用 Builder() 方法
// Head请求json
func (client *Client) HeadJSON(ctx context.Context, url string, queryParams *UrlValue, headers *Header) (resp *Response) {
	return client.fetchJSON(ctx, http.MethodHead, url, queryParams, headers, nil)
}

// Deprecated: 推荐使用 Builder() 方法
// Get请求
func (client *Client) Get(ctx context.Context, url string, queryParams *UrlValue, headers *Header) (resp *Response) {
	return client.Builder().
		Method(http.MethodGet).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		Fetch(ctx)
}

// Deprecated: 推荐使用 Builder() 方法
// Post请求
func (client *Client) Post(ctx context.Context, url string, queryParams *UrlValue, body []byte, headers *Header) (resp *Response) {
	return client.Builder().
		Method(http.MethodPost).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		Body(body).
		Fetch(ctx)
}

// Deprecated: 推荐使用 Builder() 方法
// Patch请求
func (client *Client) Patch(ctx context.Context, url string, queryParams *UrlValue, body []byte, headers *Header) (resp *Response) {
	return client.Builder().
		Method(http.MethodPatch).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		Body(body).
		Fetch(ctx)
}

// Deprecated: 推荐使用 Builder() 方法
// Put请求
func (client *Client) Put(ctx context.Context, url string, queryParams *UrlValue, body []byte, headers *Header) (resp *Response) {
	return client.Builder().
		Method(http.MethodPut).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		Body(body).
		Fetch(ctx)
}

// Deprecated: 推荐使用 Builder() 方法
// Delete请求
func (client *Client) Delete(ctx context.Context, url string, queryParams *UrlValue, headers *Header) (resp *Response) {
	return client.Builder().
		Method(http.MethodDelete).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		Fetch(ctx)
}

// Deprecated: 推荐使用 Builder() 方法
// Head请求
func (client *Client) Head(ctx context.Context, url string, queryParams *UrlValue, headers *Header) (resp *Response) {
	return client.Builder().
		Method(http.MethodHead).
		URL(url).
		QueryParams(queryParams).
		Headers(headers).
		Fetch(ctx)
}

// 关闭当前客户端
func (client *Client) Close() error {
	client.globalCancelFunc()
	client.logger.Close()
	return nil
}
