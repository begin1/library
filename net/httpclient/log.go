package httpclient

import (
	"fmt"
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const (
	_infoFile = "httpClientInfo.log"

	defaultPattern = "%J{taTUSH}"
)

type logger interface {
	Print(m map[string]interface{})
	Close()
}

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
}

func (logger Logger) Print(m map[string]interface{}) {
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Close() {
	for _, writer := range logger.writers {
		writer.Render.Close()
	}
}

// 获取默认writer
func getDefaultWriter(conf *Config) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers: writers,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"S": source,
	"a": startTime,
	"U": contextUUID,
	"t": title,
	"H": httpClientExtra,
	"s": statusCode,
	"B": responseBody,
	"D": duration,
	"e": endpoint,
	"u": urlFuc,
	"h": headersFuc,
	"b": requestBodyFuc,
	"M": methodName,
	"E": extraMessage,
}

// 结束时间
func longTime(args map[string]interface{}) (string, interface{}) {
	t, ok := args["end_time"].(time.Time)
	if !ok {
		return "time", ""
	}
	return "time", t.Format("2006/01/02 15:04:05.000")
}

// 开始时间
func startTime(args map[string]interface{}) (string, interface{}) {
	t, ok := args["start_time"].(time.Time)
	if !ok {
		return "start_time", ""
	}
	return "start_time", t.Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "HTTPCLIENT"
}

// 日志打印的调用源
func source(args map[string]interface{}) (string, interface{}) {
	return "source", args["source"]
}

// context中的uuid
func contextUUID(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args["uuid"].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 汇总的http client参数
func httpClientExtra(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){
		statusCode, responseBody, duration, urlFuc,
		headersFuc, requestBodyFuc, methodName,
		endpoint, extraMessage,
	}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "httpclient", m
}

// 额外信息
func extraMessage(args map[string]interface{}) (string, interface{}) {
	message, ok := args["extra_message"].(string)
	if !ok {
		return "extra_message", ""
	}

	return "extra_message", message
}

// 状态码
func statusCode(args map[string]interface{}) (string, interface{}) {
	code, ok := args["status_code"].(int)
	if !ok {
		return "status_code", -1
	}

	return "status_code", code
}

// 响应body
func responseBody(args map[string]interface{}) (string, interface{}) {
	body, ok := args["response_body"].(string)
	if !ok {
		return "response_body", "unknown"
	}

	return "response_body", body
}

// 请求时间
func duration(args map[string]interface{}) (string, interface{}) {
	originValue, ok := args["duration"].(time.Duration)
	if !ok {
		return "duration", 0.0
	}
	duration, err := strconv.ParseFloat(
		fmt.Sprintf("%.2f", float64(originValue.Nanoseconds()/1e4)/100.0),
		64,
	)
	if err != nil {
		return "duration", 0.0
	}
	return "duration", duration
}

// 请求url
func endpoint(args map[string]interface{}) (string, interface{}) {
	return "endpoint", args["endpoint"]
}

// 请求url
func urlFuc(args map[string]interface{}) (string, interface{}) {
	return "url", args["url"]
}

// 请求头
func headersFuc(args map[string]interface{}) (string, interface{}) {
	return "headers", args["headers"]
}

// 请求body
func requestBodyFuc(args map[string]interface{}) (string, interface{}) {
	body, ok := args["request_body"].(string)
	if !ok {
		return "request_body", "unknown"
	}

	return "request_body", body
}

// 请求方法
func methodName(args map[string]interface{}) (string, interface{}) {
	return "method_name", args["method_name"]
}
