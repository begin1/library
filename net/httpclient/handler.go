package httpclient

import (
	"fmt"
	"gitlab.com/begin1/library/net/errcode"
	"gitlab.com/begin1/library/net/tracing"
	"github.com/pkg/errors"
	"time"
)

// 处理方法
type HandlerFunc func(*Span)

// 原始Http处理方法
func GetOriginHttpHandler() HandlerFunc {
	return func(b *Span) {
		b.startTime = time.Now()
		resp, err := b.client.client.Do(b.Request)
		b.endTime = time.Now()
		b.duration = b.endTime.Sub(b.startTime)
		b.Response = resp
		b.SetError(err)
	}
}

// k8s客户端负载均衡处理方法
func GetK8sLoadBalancerHandler() HandlerFunc {
	return func(b *Span) {
		if b.GetConfig().EnableLoadBalancer {
			host := b.Request.URL.Host
			ctx := b.GetContext()
			lb := b.client.loadBalancer

			if lb.IsAdded(ctx, host) {
				_ = lb.Watch(ctx, host)
			} else {
				_ = lb.Add(ctx, host)
			}

			b.Request.URL.Host = lb.MustGetEndpoint(ctx, host)
		}

		b.Next()
	}
}

// 过滤处理方法
func GetFilterHandler() HandlerFunc {
	return func(b *Span) {
		b.Next()

		filterFunc := b.GetFilterFunc()
		if filterFunc != nil {
			err := filterFunc(b.Request, b.Response)
			b.SetError(err)
		} else {
			GetAccessStatusCodeHandler()(b)
		}
	}
}

// 状态码过滤处理方法
func GetAccessStatusCodeHandler() HandlerFunc {
	return func(b *Span) {
		b.Next()

		resp := b.Response
		if resp == nil {
			return
		}

		isAccess := len(b.GetAccessStatusCode()) == 0
		for _, code := range b.GetAccessStatusCode() {
			if resp.StatusCode == code {
				isAccess = true
				break
			}
		}
		if !isAccess {
			b.SetError(errors.New(fmt.Sprintf("status code is %d , can't access", resp.StatusCode)))
		}
	}
}

// 断路器处理方法
func GetBreakerHandler() HandlerFunc {
	return func(b *Span) {
		if b.GetConfig().DisableBreaker {
			b.Next()
			return
		}

		breaker, err := b.GetUrlBreaker(b.url)
		if err != nil {
			b.SetError(err)
			return
		}

		err = breaker.Call(b.GetContext(), func() error {
			b.Next()
			return b.GetError()
		}, 0)
		if errcode.EqualError(errcode.BreakerOpenError, err) && b.GetDegradedResponse() != nil {
			b.Response = b.GetDegradedResponse()
			b.SetError(errors.Wrap(errcode.BreakerDegradedError, err.Error()))
		} else if err != nil {
			b.SetError(err)
		}
	}
}

// 日志处理方法
func GetLogHandler() HandlerFunc {
	return func(b *Span) {
		logMsg := b.preLog()

		b.Next()

		b.log(logMsg)
	}
}

// 统计处理方法
func GetMetricsHandler() HandlerFunc {
	return func(b *Span) {
		b.metricBefore()
		b.Next()
		b.metricAfter()
	}
}

// 链路跟踪处理方法
func GetTracingHandler() HandlerFunc {
	return func(b *Span) {
		if b.GetConfig().DisableTracing {
			b.Next()
			return
		}

		ctx, span := tracing.HttpTracingBefore(b.ctx, b.Request)
		b.ctx = ctx

		b.Next()

		tracing.HttpTracingAfter(span, b.Response, b.GetError())
	}
}
