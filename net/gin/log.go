package gin

import (
	"bytes"
	"fmt"
	_context "gitlab.com/begin1/library/base/context"
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const (
	_infoFile  = "ginInfo.log"
	_errorFile = "ginError.log"

	defaultPattern = "%J{tbTUG}"

	defaultRouterPattern = "%J{tTMPNl}"

	QTHeaderUserIDKey      = "QT-User-Id"
	QTHeaderDeviceIDKey    = "Qt-Device-Id"
	QTHeaderAccessTokenKey = "QT-Access-Token"

	LogExtraKey = "LogExtraKey"
)

var (
	_logger *Logger
)

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
}

func (logger Logger) Print(m map[string]interface{}) {
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Close() {
	for _, writer := range logger.writers {
		writer.Render.Close()
	}
}

// 获取默认logger
func getDefaultRouterLogger(conf *Config) *Logger {
	if conf.Config == nil {
		conf.Config = &render.Config{}
	}

	if conf.StdoutRouterPattern == "" {
		conf.StdoutRouterPattern = defaultRouterPattern
	}
	if conf.OutRouterPattern == "" {
		conf.OutRouterPattern = defaultRouterPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutRouterPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutRouterPattern),
		})
	}

	return &Logger{
		writers: writers,
	}
}

// 获取info级别writer
func GetInfoWriter(conf *Config) io.Writer {
	return getGinWriter(conf, _infoFile)
}

// 获取error级别writer
func GetErrorWriter(conf *Config) io.Writer {
	return getGinWriter(conf, _errorFile)
}

// 获取writer
func getGinWriter(conf *Config, name string) io.Writer {
	if conf.Config == nil {
		conf.Config = &render.Config{}
	}

	var writers []io.Writer
	if conf.Stdout {
		writers = append(writers, os.Stdout)
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, name),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, fw)
	}

	return io.MultiWriter(writers...)
}

// 获取默认格式化渲染器
// todo:只能打印控制台或文件，不能同时打印
func GetDefaultFormatter(conf *Config) gin.HandlerFunc {
	if conf.Config == nil {
		conf.Config = &render.Config{}
	}

	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	ginRender := render.NewPatternRender(patternMap, conf.StdoutPattern)
	return func(c *gin.Context) {
		relativePath := GetGinRelativePath(c)

		var header http.Header
		var requestBody string
		request := c.Request
		if request != nil {
			header = request.Header
			if conf.RequestBodyOut {
				bodyBytes, err := ioutil.ReadAll(request.Body)
				if err != nil {
					goto SkipRequest
				}
				err = request.Body.Close()
				if err != nil {
					goto SkipRequest
				}
				request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
				requestBody = string(bodyBytes)
			}
		}

	SkipRequest:
		gin.LoggerWithFormatter(
			func(param gin.LogFormatterParams) string {
				msg := make(map[string]interface{})
				msg["time"] = param.TimeStamp
				msg["uuid"] = c.Value(_context.ContextUUIDKey)
				msg["status_code"] = param.StatusCode
				msg["latency"] = float64(param.Latency) / float64(time.Millisecond)
				msg["start_time"] = param.TimeStamp.Add(-param.Latency)
				msg["client_ip"] = param.ClientIP
				msg["method"] = param.Method
				msg["path"] = param.Path
				msg["relative_path"] = relativePath
				msg["error_message"] = param.ErrorMessage
				msg["header"] = header
				msg["request_body"] = requestBody
				msg["extra"] = c.Value(LogExtraKey)

				return ginRender.RenderString(msg)
			},
		)(c)
	}
}

func GetDefaultRouterPrintFunc(conf *Config) func(httpMethod, absolutePath, handlerName string, nuHandlers int) {
	if conf == nil {
		conf = &Config{
			Config: &render.Config{
				Stdout: true,
			},
		}
	}
	if conf.Config == nil {
		conf.Config = &render.Config{}
	}

	_logger = getDefaultRouterLogger(conf)

	return func(httpMethod, absolutePath, handlerName string, nuHandlers int) {
		msg := make(map[string]interface{})
		msg["time"] = time.Now()
		msg["method"] = httpMethod
		msg["path"] = absolutePath
		msg["handler_name"] = handlerName
		msg["handler_length"] = nuHandlers

		_logger.Print(msg)
	}
}

// 注入自定义日志
func SetCustomLog(c *gin.Context, v interface{}) {
	c.Set(LogExtraKey, v)
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	// 通用支持字符
	"T": longTime,
	"t": title,
	"M": method,
	"P": path,
	// 普通日志支持字符
	"U": contextUUID,
	"G": ginExtra,
	"s": statusCode,
	"b": startTime,
	"L": latency,
	"C": clientIP,
	"E": errorMessage,
	"e": extra,
	"R": relativePath,
	"H": header,
	"B": requestBody,
	// 路由日志支持字符
	"N": handlerName,
	"l": handlerLength,
}

// 当前时间
func longTime(args map[string]interface{}) (string, interface{}) {
	return "time", args["time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 开始时间
func startTime(args map[string]interface{}) (string, interface{}) {
	return "start_time", args["start_time"].(time.Time).Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "GIN"
}

// context中的uuid
func contextUUID(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args["uuid"].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 汇总的gin参数
func ginExtra(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){
		statusCode, latency, clientIP, method,
		path, relativePath, header, requestBody, errorMessage,
		extra,
	}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "gin", m
}

// 状态码
func statusCode(args map[string]interface{}) (string, interface{}) {
	return "status_code", args["status_code"]
}

// 请求时间
func latency(args map[string]interface{}) (string, interface{}) {
	originValue := args["latency"].(float64)
	duration, err := strconv.ParseFloat(fmt.Sprintf("%.2f", originValue), 64)
	if err != nil {
		duration = originValue
	}
	return "latency", duration
}

// 客户端ip
func clientIP(args map[string]interface{}) (string, interface{}) {
	return "client_ip", args["client_ip"]
}

// 请求方法
func method(args map[string]interface{}) (string, interface{}) {
	return "method", args["method"]
}

// 请求路径
func path(args map[string]interface{}) (string, interface{}) {
	return "path", args["path"]
}

// 错误信息
func errorMessage(args map[string]interface{}) (string, interface{}) {
	return "error_message", args["error_message"]
}

// 自定义参数
func extra(args map[string]interface{}) (string, interface{}) {
	return "extra", args["extra"]
}

// 路由方法
func relativePath(args map[string]interface{}) (string, interface{}) {
	return "relative_path", args["relative_path"]
}

// 请求头
func header(args map[string]interface{}) (string, interface{}) {
	return "header", args["header"]
}

// 请求体
func requestBody(args map[string]interface{}) (string, interface{}) {
	return "request_body", args["request_body"]
}

// 路由处理器名称
func handlerName(args map[string]interface{}) (string, interface{}) {
	return "handler_name", args["handler_name"]
}

// 路由经过的处理器数量
func handlerLength(args map[string]interface{}) (string, interface{}) {
	return "handler_length", args["handler_length"]
}
