package tracing

import (
	"context"
	"fmt"
	_context "gitlab.com/begin1/library/base/context"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"net/http"
)

// http操作前跟踪
func HttpTracingBefore(ctx context.Context, req *http.Request) (context.Context, opentracing.Span) {
	if req == nil {
		return ctx, nil
	}

	parentSpan, err := GetCurrentSpanFromContext(ctx)
	if err != nil {
		return ctx, nil
	}

	span := parentSpan.Tracer().StartSpan(
		fmt.Sprintf("%s%s", SpanPrefixHttpClient, req.URL.Host),
		opentracing.ChildOf(parentSpan.Context()),
	)
	err = span.Tracer().Inject(span.Context(), opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(req.Header))
	if err != nil {
		_logger.Error(fmt.Sprintf("%s", err))
		return ctx, nil
	}
	ext.HTTPMethod.Set(span, req.Method)
	ext.HTTPUrl.Set(span, req.URL.String())

	ctx = SetCurrentSpanToContext(ctx, span)
	return ctx, span
}

// http操作后跟踪
func HttpTracingAfter(span opentracing.Span, resp *http.Response, err error) {
	if span == nil {
		return
	}
	defer span.Finish()

	if err != nil {
		span.SetTag("http.error", err)
		ext.Error.Set(span, true)
	}

	if resp != nil {
		statusCode := resp.StatusCode
		ext.HTTPStatusCode.Set(span, uint16(statusCode))
		if statusCode != http.StatusOK {
			ext.Error.Set(span, true)
		}
	}
}

// goroutine操作前跟踪
func GoroutineTracingBefore(ctx context.Context, groupName, goroutineName, mode string) (context.Context, opentracing.Span) {
	parentSpan, err := GetCurrentSpanFromContext(ctx)
	if err != nil {
		return ctx, nil
	}

	spanContext := parentSpan.Context()
	span := parentSpan.Tracer().StartSpan(
		fmt.Sprintf("%s%s", SpanPrefixGoroutine, goroutineName),
		opentracing.ChildOf(spanContext),
	)
	span.SetTag("go.group", groupName)
	span.SetTag("go.goroutine", goroutineName)
	span.SetTag("go.mode", mode)
	span.SetTag("uuid", _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown"))

	ctx = SetCurrentSpanToContext(ctx, span)
	return ctx, span
}

// goroutine操作后跟踪
func GoroutineTracingAfter(span opentracing.Span, err error) {
	if span == nil {
		return
	}
	defer span.Finish()

	if err != nil {
		ext.Error.Set(span, true)
		span.SetTag("go.error", err)
	}
}
