package tracing

import (
	"context"
	"fmt"
	_context "gitlab.com/begin1/library/base/context"
	"github.com/jinzhu/gorm"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

// gorm操作前跟踪
func GormTracingBefore(ctx context.Context, dsn string, operation string) (context.Context, opentracing.Span) {
	parentSpan, err := GetCurrentSpanFromContext(ctx)
	if err != nil {
		return ctx, nil
	}

	spanContext := parentSpan.Context()
	span := parentSpan.Tracer().StartSpan(
		fmt.Sprintf("%s%s", SpanPrefixGorm, operation),
		opentracing.ChildOf(spanContext),
	)
	span.SetTag("uuid", _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown"))
	ext.DBType.Set(span, "mysql")
	ext.DBInstance.Set(span, dsn)

	ctx = SetCurrentSpanToContext(ctx, span)
	return ctx, span
}

// gorm操作后跟踪
func GormTracingAfter(span opentracing.Span, scope *gorm.Scope, fullSql, operation string) {
	if span == nil {
		return
	}
	defer span.Finish()

	span.SetTag("db.method", operation)
	span.SetTag("db.table", scope.TableName())
	span.SetTag("db.rows", scope.DB().RowsAffected)
	ext.DBStatement.Set(span, fullSql)

	if scope.HasError() {
		ext.Error.Set(span, true)
		span.SetTag("db.error", scope.DB().Error)
	}
}

// redis操作前跟踪
func RedisTracingBefore(ctx context.Context, commandName string, endpoint string) (context.Context, opentracing.Span) {
	parentSpan, err := GetCurrentSpanFromContext(ctx)
	if err != nil {
		return ctx, nil
	}

	spanContext := parentSpan.Context()
	span := parentSpan.Tracer().StartSpan(
		fmt.Sprintf("%s%s", SpanPrefixRedis, commandName),
		opentracing.ChildOf(spanContext),
	)
	span.SetTag("uuid", _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown"))
	ext.DBType.Set(span, "redis")
	ext.DBInstance.Set(span, endpoint)
	ext.DBStatement.Set(span, commandName)

	ctx = SetCurrentSpanToContext(ctx, span)
	return ctx, span
}

// redis操作后跟踪
func RedisTracingAfter(span opentracing.Span, err error) {
	if span == nil {
		return
	}
	defer span.Finish()

	if err != nil {
		ext.Error.Set(span, true)
		span.SetTag("db.error", err)
	}
}

// mongo操作前跟踪
func MongoTracingBefore(ctx context.Context, funcName, collectionName string) (context.Context, opentracing.Span) {
	parentSpan, err := GetCurrentSpanFromContext(ctx)
	if err != nil {
		return ctx, nil
	}

	spanContext := parentSpan.Context()
	span := parentSpan.Tracer().StartSpan(
		fmt.Sprintf("%s%s", SpanPrefixMongo, funcName),
		opentracing.ChildOf(spanContext),
	)
	span.SetTag("uuid", _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown"))
	ext.DBType.Set(span, "mongo")
	ext.DBStatement.Set(span, funcName)
	span.SetTag("db.collection", collectionName)

	ctx = SetCurrentSpanToContext(ctx, span)
	return ctx, span
}

// mongo操作后跟踪
func MongoTracingAfter(span opentracing.Span, err error) {
	if span == nil {
		return
	}
	defer span.Finish()

	if err != nil {
		ext.Error.Set(span, true)
		span.SetTag("db.error", err)
	}
}

// redlock操作前跟踪
func RedlockTracingBefore(ctx context.Context, mutexName, commandName string) (context.Context, opentracing.Span) {
	parentSpan, err := GetCurrentSpanFromContext(ctx)
	if err != nil {
		return ctx, nil
	}

	spanContext := parentSpan.Context()
	span := parentSpan.Tracer().StartSpan(
		fmt.Sprintf("%s%s", SpanPrefixRedlock, mutexName),
		opentracing.ChildOf(spanContext),
	)
	span.SetTag("uuid", _context.GetStringOrDefault(ctx, _context.ContextUUIDKey, "unknown"))
	ext.DBType.Set(span, "redlock")
	ext.DBStatement.Set(span, commandName)
	ext.DBInstance.Set(span, mutexName)

	ctx = SetCurrentSpanToContext(ctx, span)
	return ctx, span
}

// redlock操作后跟踪
func RedlockTracingAfter(span opentracing.Span, err error) {
	if span == nil {
		return
	}
	defer span.Finish()

	if err != nil {
		ext.Error.Set(span, true)
		span.SetTag("db.error", err)
	}
}
