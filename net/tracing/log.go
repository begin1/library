package tracing

import (
	"fmt"
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"io"
	"os"
	"path/filepath"
	"time"
)

const (
	_infoFile = "jaegerInfo.log"

	ErrorLevel = "ERROR"
	InfoLevel  = "INFO"

	defaultPattern = "%J{tTE}"
)

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
}

// 实现jaeger中logger接口
func (logger Logger) Infof(msg string, args ...interface{}) {
	m := make(map[string]interface{})
	m["level"] = InfoLevel
	m["message"] = fmt.Sprintf(msg, args...)
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Error(msg string) {
	m := make(map[string]interface{})
	m["level"] = ErrorLevel
	m["message"] = msg
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

// 获取默认writer
func getDefaultWriter(conf *Config) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers: writers,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"t": title,
	"E": tracingExtra,
	"L": level,
	"M": message,
}

// 当前时间
func longTime(map[string]interface{}) (string, interface{}) {
	return "time", time.Now().Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "OPENTRACING"
}

// 汇总的tracing参数
func tracingExtra(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){level, message}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "opentracing", m
}

// 日志级别
func level(args map[string]interface{}) (string, interface{}) {
	return "level", args["level"]
}

// 日志消息
func message(args map[string]interface{}) (string, interface{}) {
	return "message", args["message"]
}
