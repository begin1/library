package log

import (
	"fmt"
	"strings"
	"sync"
	"time"
)

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"L": keyFactory("level", _level),
	"S": keyFactory("source", _source),
	"M": textMessage,
	"m": jsonMessage,
	"U": uuid,
	"t": title,
}

// 基础key加工
func keyFactory(field, key string) func(map[string]interface{}) (string, interface{}) {
	return func(d map[string]interface{}) (string, interface{}) {
		if v, ok := d[key]; ok {
			if s, ok := v.(string); ok {
				return field, s
			}
			return field, fmt.Sprint(v)
		}
		return field, ""
	}
}

// 当前时间
func longTime(map[string]interface{}) (string, interface{}) {
	return "time", time.Now().Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "LOG"
}

// context中的uuid
func uuid(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args[_uuid].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 是否是内部键，内部键不打印到消息主体中
func isInternalKey(k string) bool {
	switch k {
	case _level, _levelValue, _time, _source, _appID, _uuid:
		return true
	}
	return false
}

// 消息主体,用于json形式
func jsonMessage(args map[string]interface{}) (string, interface{}) {
	var m string
	msgMap := make(map[string]interface{})

	for k, v := range args {
		if k == _log {
			m = fmt.Sprint(v)
			continue
		}

		// 添加非内部键值对
		if isInternalKey(k) {
			continue
		}
		msgMap[k] = v
	}

	if m != "" {
		// 追加消息主体
		msgMap["log"] = m
	}

	return "message", msgMap
}

// 消息切片对象池
var messageSlicePool = sync.Pool{
	New: func() interface{} {
		return make([]string, 0)
	},
}

// 消息主体,用于text形式
func textMessage(args map[string]interface{}) (string, interface{}) {
	var m string
	s := messageSlicePool.Get().([]string)

	for k, v := range args {
		if k == _log {
			m = fmt.Sprint(v)
			continue
		}

		// 添加非内部键值对
		if isInternalKey(k) {
			continue
		}
		s = append(s, fmt.Sprintf("%s=%v", k, v))
	}
	// 追加消息主体
	msg := strings.Join(append(s, m), " ")

	// reset 对象池
	messageSlicePool.Put(s[0:0])

	return "message", msg
}
