package goroutine

import render "gitlab.com/begin1/library/base/logrender"

var (
	conf *Config
)

// 默认初始化
func init() {
	conf = &Config{
		Config: &render.Config{
			Stdout: false,
			OutDir: "",
		},
	}
}

type Config struct {
	// 日志配置
	*render.Config `yaml:",inline"`
}

// 外部初始化
func Init(c *Config) {
	if c != nil {
		conf = c
	}

	if conf.Config == nil {
		conf.Config = &render.Config{}
	}

	_logger = getDefaultLogger(conf)
}

// 关闭
func Close() error {
	_logger.Close()
	return nil
}
