package goroutine

import (
	"gitlab.com/begin1/library/base/filewriter"
	render "gitlab.com/begin1/library/base/logrender"
	"io"
	"os"
	"path/filepath"
	"time"
)

const (
	_infoFile      = "goroutineInfo.log"
	defaultPattern = "%J{tTUSG}"
)

type logger interface {
	Print(m map[string]interface{})
	Close()
}

type LogWriter struct {
	io.Writer
	render.Render
}

type Logger struct {
	writers []LogWriter
}

func (logger Logger) Print(m map[string]interface{}) {
	for _, writer := range logger.writers {
		writer.Render.Render(writer.Writer, m)
	}
}

func (logger Logger) Close() {
	for _, writer := range logger.writers {
		writer.Render.Close()
	}
}

// 获取默认logger
func getDefaultLogger(conf *Config) *Logger {
	if conf.StdoutPattern == "" {
		conf.StdoutPattern = defaultPattern
	}
	if conf.OutPattern == "" {
		conf.OutPattern = defaultPattern
	}

	var writers []LogWriter
	if conf.Stdout {
		writers = append(writers, LogWriter{
			Writer: os.Stdout,
			Render: render.NewPatternRender(patternMap, conf.StdoutPattern),
		})
	}
	if conf.OutDir != "" {
		fw := filewriter.NewSingleFileWriter(
			filepath.Join(conf.OutDir, _infoFile),
			conf.FileBufferSize, conf.RotateSize, conf.MaxLogFile,
		)
		writers = append(writers, LogWriter{
			Writer: fw,
			Render: render.NewPatternRender(patternMap, conf.OutPattern),
		})
	}

	return &Logger{
		writers: writers,
	}
}

var patternMap = map[string]func(map[string]interface{}) (string, interface{}){
	"T": longTime,
	"S": source,
	"U": contextUUID,
	"t": title,
	"G": goroutine,
	"s": state,
	"N": groupName,
	"n": goroutineName,
	"I": groupID,
	"i": goroutineID,
	"E": extra,
	"m": mode,
}

// 当前时间
func longTime(map[string]interface{}) (string, interface{}) {
	return "time", time.Now().Format("2006/01/02 15:04:05.000")
}

// 日志标题
func title(map[string]interface{}) (string, interface{}) {
	return "title", "GOROUTINE"
}

// 日志打印的调用源
func source(args map[string]interface{}) (string, interface{}) {
	return "source", args["source"]
}

// context中的uuid
func contextUUID(args map[string]interface{}) (string, interface{}) {
	uuid, ok := args["uuid"].(string)
	if !ok {
		return "uuid", "unknown"
	}

	return "uuid", uuid
}

// 汇总的goroutine参数
func goroutine(args map[string]interface{}) (string, interface{}) {
	m := make(map[string]interface{})

	funcArray := []func(map[string]interface{}) (string, interface{}){
		state, groupName, groupID, goroutineID, goroutineName, extra, mode,
	}
	for _, f := range funcArray {
		k, v := f(args)
		m[k] = v
	}

	return "goroutine", m
}

// 协程状态
func state(args map[string]interface{}) (string, interface{}) {
	return "state", args["state"]
}

// 协程组名
func groupName(args map[string]interface{}) (string, interface{}) {
	return "group_name", args["group_name"]
}

// 协程组id
func groupID(args map[string]interface{}) (string, interface{}) {
	return "group_id", args["group_id"]
}

// 协程名
func goroutineName(args map[string]interface{}) (string, interface{}) {
	return "goroutine_name", args["goroutine_name"]
}

// 协程id
func goroutineID(args map[string]interface{}) (string, interface{}) {
	return "goroutine_id", args["goroutine_id"]
}

// 额外参数，如报错信息
func extra(args map[string]interface{}) (string, interface{}) {
	return "extra", args["extra"]
}

// 协程组模式
func mode(args map[string]interface{}) (string, interface{}) {
	return "mode", args["mode"]
}
